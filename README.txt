Download and connect the repository:

1. Create Unity project (Name it what ever the fuck you want!)
2. Have admin (Alex Hollums) add your email to the repository
3. Open SourceTree
4. File/New -> Clone Repository
5. Source URL: https://bitbucket.org/ahollums/ggj_hsv.git
6. Destination URL: Make sure the destination directory is your Assets folder in the Unity project just created.
6a. If SourceTree is being a "BITCH", then enable Git Settings in the Tools tab and go to Git and install for SourceTree.
7. In SourceTree, go to Repository/Settings then click Advanced tab
8. Fill in your name and email you registered to bitbucket with.


Add your contact info to the README:

1. Click the pull button at the top to make sure you’re up to date.
2. Open the README.txt file and add info
3. Go to SourceTree and click Commit
4. Click the check box next to the README.txt file in the box that says “Unstaged files”
5. Add a message about you adding your info in the Commit message box at the bottom.
6. Check the box below the message filed “Push changes immediately…” 
7. Hit the commit button.

Alex Hollums - ahollums@gmail.com - 678 633 9385
Nick Compton - ncompton13@aol.com - 614 209 5560
Derek Lewis - dereklewis12@gmail.com - 740 550 9849
Chris George - soccergeorge10@gmail.com - 513 310 4620