﻿using UnityEngine;
using System.Collections;
using System.IO;

[System.Serializable]
public enum OCDFileSubject
{
	HIGHSCORE = 0,
	DAYS_ENDURED
}

public class OCDFileParser 
{
	public static string highscoreDoc = "OCD_Highscore.txt";
	static string contentSeparator = "\t\t\t";
	public string documentName;

	public bool usePlayerPrefs = true;

	public OCDFileParser(string pDocumentName)
	{
		if (usePlayerPrefs)
			return;
		
		documentName = pDocumentName;
		if(!DocumentExists())
		{
			if(FileTools.CreateFile(documentName))
			{
				ResetData();
			}
			else
			{
				Log(string.Format("Error creating the file [{0}]", documentName), LogType.Warning);
			}
		}
	}

	public bool DocumentExists()
	{
		return File.Exists(documentName);
	}

	public void ResetData()
	{
		WriteSubject(OCDFileSubject.HIGHSCORE, 0);
		WriteSubject(OCDFileSubject.DAYS_ENDURED, 0);
	}
		
	public int ReadSubject(OCDFileSubject subject)
	{
		if (usePlayerPrefs) {
			if (UnityEngine.PlayerPrefs.HasKey (subject.ToString ())) {
				return UnityEngine.PlayerPrefs.GetInt (subject.ToString ());
			} else {
				return 0;
			}
		}
		else return ReadIntValue(subject.ToString());
	}

	int ReadIntValue(string subject)
	{
		string line = GetLine(subject, documentName);
		if(string.IsNullOrEmpty(line)) 
		{
			Log(string.Format("Could not find line with subject [{0}] in the document [{1}].", subject, documentName), LogType.Warning);
			return -1;
		}
		string rawValue = GetLineValue (line);
		int value = 0;
		if(int.TryParse(rawValue, out value)) return value;
		else
		{
			Log(string.Format("Failed to parse value from line [{0}]", line));
			return -1;
		}
	}

	static string GetLine(string searchString, string pDocumentName)
	{
		string fileContents = FileTools.ReadFrom(pDocumentName);
		if(string.IsNullOrEmpty(fileContents))
		{
			Log(string.Format("There was an error reading from the file [{0}].", pDocumentName), LogType.Error);
			return null;
		}
		string[] fileLines = fileContents.Split(new string[] { "\n" }, System.StringSplitOptions.RemoveEmptyEntries);
		foreach(string s in fileLines)
		{
			if(s.ToLower().Contains (searchString.ToLower()))
			{
				return s;
			}
		}

		Log(string.Format("Couldn't find any lines preceded by [{0}] in the document [{1}] with contents [{2}]", 
			searchString, pDocumentName, fileContents), LogType.Error);
		return null;
	}

	static string GetLineValue(string line)
	{
		string[] splitLine = line.Split(new string[] { contentSeparator }, System.StringSplitOptions.RemoveEmptyEntries);
		if(splitLine.Length < 2)
		{
			return null;
		}
		return splitLine[1];
	}

	public bool WriteSubject(OCDFileSubject subject, int value)
	{
		if (usePlayerPrefs) { 
			UnityEngine.PlayerPrefs.SetInt (subject.ToString (), value);
			return true;
		}
		else return WriteIntValue (subject.ToString(), value);
	}

	public bool WriteIntValue (string subject, int value)
	{
		string contents = FileTools.ReadFrom(documentName);
		string newLine = subject + contentSeparator + value.ToString();
		string[] lines = contents.Split(new string[] { "\n" }, System.StringSplitOptions.RemoveEmptyEntries);
		bool replacingLine = false;
		for(int i=0;i<lines.Length;i++)
		{
			if(lines[i].Contains(subject))
			{
				replacingLine = true;
				lines[i] = newLine;
			}
		}

		if(!replacingLine)
		{
			if(!FileTools.WriteTo (documentName, newLine, true))
			{
				Log (string.Format("Failed to write [{0}] to the document [{1}].", newLine, documentName), LogType.Warning);
				return false;
			}else return true;
		}
		else
		{
			string reformedContents = "";
			foreach(string line in lines) reformedContents+=line+"\n";
			if(!FileTools.WriteTo (documentName, reformedContents, false))
			{
				Log (string.Format("Failed to write [{0}] to the document [{1}].", string.Concat(lines), documentName), LogType.Warning);
				return false;
			}
			else return true;
		}
	}

	static void Log(string message, LogType type = LogType.Log)
	{
		message = "[OCDFileParser]: "+message;
		switch(type)
		{
		case LogType.Log:
			Debug.Log(message);
			break;
		case LogType.Warning:
			Debug.LogWarning(message);
			break;
		case LogType.Error:
			Debug.LogError(message);
			break;
		}
	}
}
