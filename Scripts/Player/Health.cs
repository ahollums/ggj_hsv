﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic; 

public class Health : MonoBehaviour {

	public Sprite sprite;
	public Text healthText;
	public float maxHealth;
	[SerializeField] private float health;
	public float GetHealth{get{return health;}}
	private Image healthBar;
	public int myLives;
	public List<GameObject> healthIcons;


	void Awake () {

		healthIcons = new List<GameObject> ();
		//healthBar = transform.FindChild("HealthBG").FindChild("Health").GetComponent<Image>();
		//GetComponent<SpriteRenderer>().enabled = false;

	}
	void Update(){

		//for testing 
		if(Input.GetKeyUp(KeyCode.Space)){
			Hit(1);
		}

	}

	public void Hit(int amount){

		//for a visual healthbar just need to set up amounts.
		//healthBar.transform.localScale = Vector3.Lerp(healthBar.transform.localScale, new Vector3(0,healthBar.transform.localScale.y,healthBar.transform.localScale.y), 1* Time.deltaTime);

		if (healthIcons.Count > 0) { // Here because errors happen if there are no icons
			//this could def be written better. Being populated by grid script
			foreach (GameObject GO in healthIcons) {
				GO.SetActive (false);
			}

			healthIcons.RemoveRange (healthIcons.Count-1, amount);//ideally de-activate it right here

			foreach (GameObject GO in healthIcons) {
				GO.SetActive (true);
			}
		}

		HitPoints = health - amount;

	}

    public float HitPoints
    {
        set
        {
            health = value;
//            healthText.text = health.ToString();
            UIManager.HitText = value.ToString();
        }
    }

	public void Die(){
		Debug.Log ("Dead");
	}
}
