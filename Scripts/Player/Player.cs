﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour 
{
    public static Player instance;
    public Health health;
    [SerializeField] int objectivesCompleted = 0;
	[SerializeField] int medications = 3;
	static int HIGHSCORE = 0;
	static int DAYS_ENDURED = 0;
	const int MIN_HIGHSCORE = 1;
	public static bool playedHighScoreClip = false;
	OCDFileParser fileReader;

    void Awake()
    {
        instance = this;
        health.HitPoints = 3f;
		fileReader = new OCDFileParser(OCDFileParser.highscoreDoc);
		HIGHSCORE = fileReader.ReadSubject(OCDFileSubject.HIGHSCORE);
		DAYS_ENDURED = fileReader.ReadSubject(OCDFileSubject.DAYS_ENDURED);
		playedHighScoreClip = false;
    }

    public static Health PlayerHealth
    {
        get
        {
            return instance ? instance.health : null;
        }
    }

    public static int Score
    {
        get
        {
            return instance.objectivesCompleted;
        }
        set
        {
            instance.objectivesCompleted = value;
			if(value > HIGHSCORE && value > MIN_HIGHSCORE)
			{
				UIManager.HighScore();
				playedHighScoreClip = true;
				HIGHSCORE = value;
				if(instance.fileReader != null)
				{
					instance.fileReader.WriteSubject(OCDFileSubject.HIGHSCORE, HIGHSCORE);
				}
			}
            UIManager.SetScore(value);
        }
    }

	public static int Medications
	{
		set
		{
			if(instance) instance.medications = value;
		}
		get
		{
			return instance ? instance.medications : 0;
		}
	}

	public static int DaysEndured
	{
		get
		{
			return DAYS_ENDURED;
		}
		set
		{
			if(value > DAYS_ENDURED)
			{
				DAYS_ENDURED = value;
				if(instance.fileReader != null)
				{
					instance.fileReader.WriteSubject(OCDFileSubject.DAYS_ENDURED, DAYS_ENDURED);
				}
			}
		}
	}
}
