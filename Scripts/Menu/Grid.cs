﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 

public class Grid : MonoBehaviour {

	public int GridSize;
	public Health healthScript;
	//List<GameObject> healthIcons;

	void Start () {

		for (int i = 0; i < GridSize; i++) {
			GameObject prefab=	Instantiate(Resources.Load ("Cube"),new Vector3(i * 2, 0, 0), Quaternion.identity) as GameObject;
			healthScript.healthIcons.Add (prefab);
		}
	}
}
