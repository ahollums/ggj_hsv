﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;
public class SceneLoader : MonoBehaviour {

	//spublic static bool pauseMe;
	public GameObject Holder;
	public bool toggle;
	public static SceneLoader instance;

	void Awake()
	{
		instance = this;
	}

	//make a delegate for pause, or function
	//make health and link up for death etc
	public static void PauseGame(bool pauseMe){
		Debug.Log ("pausing");
		if (pauseMe) {
			Time.timeScale = 0;
//			Time.unscaledTime;
		}

		
		if (!pauseMe) {
			//Time.unscaledTime = 1;
			Time.timeScale = 1;
		}
	}

	public void LoadCredits(){
		Debug.Log ("loading credits");
		PauseGame(false);
		SceneManager.LoadScene("CreditsScreen");

	}
	public void LoadGame(){
		Debug.Log ("loading game");
		PauseGame(false);
		SceneManager.LoadScene("GameLevel");

	}
	public void LoadStart(){
		Debug.Log ("loading start");
		PauseGame(false);
		SceneManager.LoadScene("StartScreen");

	}
	public void QuitApp(){
		Debug.Log ("qutting");
#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
#endif
		Application.Quit();
	}
	public void ToggleHolder(){
		toggle = !toggle;
		PauseGame(!toggle);

		if (toggle) {
//			Holder.SetActive (false);
//            ActionHandle.DelayedAction(1f, ()=> { 
//                Time.timeScale = 1;
//            });
//			SM.UnpauseAudio();
		}

		if (!toggle) {
			//goes here first, toggle is false;
//			Holder.SetActive (true);
//            ActionHandle.DelayedAction(1f, ()=> { 
//                Time.timeScale = 0;
//            });
//			SM.PauseAudio();

		}
	}

}
