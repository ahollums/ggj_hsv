﻿using UnityEngine;
using System.Collections;

public class InteractiveHighlight : InteractiveObject {
	public InteractType type = InteractType.NONE;
	public HighlightControl highlightControl;

	void Start()
	{
		if(highlightControl == null)
			highlightControl = GameObject.FindObjectOfType<HighlightControl>();

		switch(type)
		{
		case InteractType.CLICK:
			onMouseDown+=Highlight;
			onMouseUp+=UnHighlight;
			break;
		case InteractType.DRAG:
			onDragStart+=Highlight;
			onDragRelease+=UnHighlight;
			break;
		case InteractType.HOVER:
			onMouseOver+=Highlight;
			onMouseExit+=UnHighlight;
			break;
		default:
			break;
		}
	}

	void Highlight()
	{
		if(highlightControl == null) return;
		highlightControl.Highlight(gameObject);
	}

	void UnHighlight()
	{
		if(highlightControl == null) return;
		highlightControl.UnHighlight(gameObject);
	}
}
