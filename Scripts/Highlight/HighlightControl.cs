﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public enum HighlightType
{
	ADDITIVE = 0,
	REPLACE
}

public class HighlightControl : MonoBehaviour {
	public Material highlightMaterial;
	protected Dictionary<GameObject, List<Material>> dictionary = new Dictionary<GameObject, List<Material>>();
	public HighlightType type = HighlightType.ADDITIVE;

    public bool singular = false;
    protected GameObject currentHighlightedObject = null;

	protected float cacheAlpha = 0f;

	// Use this for initialization
	protected virtual void Start () {
		if(highlightMaterial != null)
			cacheAlpha = highlightMaterial.color.a;
	}
	
	public virtual void Highlight(GameObject go)
	{
		if(go == null) return;

		if(dictionary.Count == 0 || !dictionary.ContainsKey(go)) AddObjectToDictionary(go);

        if (singular)
        {
            if (currentHighlightedObject)
                UnHighlight(currentHighlightedObject);
            currentHighlightedObject = go;
        }

		foreach(KeyValuePair<GameObject, List<Material>> entry in dictionary)
		{
			if(entry.Key == go)
			{
				List<Material> mats = new List<Material>();
				switch(type)
				{
				case HighlightType.ADDITIVE:
					mats = GetMaterials (go);
					break;
				case HighlightType.REPLACE:
					break;
				default:
					break;
				}
				mats.Add (highlightMaterial);
				SetObjectMaterials(go,mats);
				foreach(Transform child in go.transform)
				{
					Highlight (child.gameObject);
				}
				break;
			}
		}
	}

	public virtual void UnHighlight(GameObject go)
	{
		if(go == null) return;

		if(go == null) return;
		
		if(dictionary.Count == 0 || !dictionary.ContainsKey(go)) AddObjectToDictionary(go);
		
		foreach(KeyValuePair<GameObject, List<Material>> entry in dictionary)
		{
			if(entry.Key == go)
			{
				List<Material> mats = entry.Value;
				SetObjectMaterials(go,mats);
				foreach(Transform child in go.transform)
				{
					UnHighlight (child.gameObject);
				}
				break;
			}
		}
	}

	protected List<Material> GetMaterials(GameObject go)
	{
		List<Material> objMats = new List<Material>();
		Renderer r = go.GetComponent<Renderer>();
		if(r !=null)
		{
			Material[] mats = r.materials;
			if(mats.Length > 0)
			{
				foreach(Material mat in mats)
				{
					objMats.Add (mat);
				}
			}
		}
		return objMats;
	}

	protected void AddObjectToDictionary(GameObject go)
	{
#if UNITY_EDITOR
		Debug.Log ("Adding to dictionary.");
#endif
		List<Material> objMats = GetMaterials (go);
		if(objMats.Count == 0)return;
		dictionary.Add (go, objMats);
	}

	protected void SetObjectMaterials(GameObject go, List<Material> mats)
	{
		if(go == null || mats.Count == 0) return;
		Renderer r = go.GetComponent<Renderer>();
		if(r!=null)
		{
			r.materials = mats.ToArray ();
		}
	}

	public void ResetMaterial()
	{
		//if(highlightMaterial != null) highlightMaterial.color.a = cacheAlpha;
	}
}
