﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public enum AudioEnum
{
    ThemeSong,
    CorrectClip,
    GameOverClip,
    HurryUpClip,
    KnobDownClip,
    KnobUpClip,
    ObjectiveAchievedClip,
    ReadyClip,
    SwitchdownClip,
    SwitchUpClip,
    WrongClip,
    YouLoseClip,
    YouWinClip,
    clickClip,
    YoureAwesomeClip,
	HighScore
}

[System.Serializable]
public enum SoundType
{
    NONE = 0,
    KNOB,
    SWITCH,
    DOOR
}

public class SoundManger : MonoBehaviour {

	//win, fail, click
	public AudioClip winAudio, failAudio, correctAudio, gameOverAudio, hurryUpAudio, objectiveAudio, readyAudio, wrongAudio, yourAwesomeAudio, clickAudio, highScoreAudio;
    public AudioClip[] audioSourcesKnob = new AudioClip[0];
    int currentKnob = 0;
    public AudioClip[] audioSourcesSwitch = new AudioClip[0];
    int currentSwitch = 0;
    public AudioClip[] audioSourcesFrustation = new AudioClip[0];
    public AudioClip[] audioSourcesDoor = new AudioClip[0];
    int currentDoor = 0;
	public AudioClip bgMusic;
	public AudioSource audioBG, otherAudio;
    public static SoundManger instance;

    public int currentFrustration = 0;

	//msuic, ui, voice commands
	void Start() {
//		AudioEnum sampleAudio;
        if (audioBG)
        {
            audioBG.clip = bgMusic;
            audioBG.volume = .5f;
            audioBG.Play();
        }
        if(otherAudio) otherAudio.volume = .5f;
        instance = this;
	}

    public static void Play(AudioEnum e)
    {
        if (instance)
        {
            instance.PlaySound(e);
        }

    }

    public static void Frustration()
    {
        if (instance == null || instance.otherAudio == null)
            return;

        instance.otherAudio.PlayOneShot(instance.audioSourcesFrustation[instance.currentFrustration%instance.audioSourcesFrustation.Length]);
        instance.currentFrustration++;
    }

    public static void PlaySoundEffect(SoundType sType)
    {
        if (instance == null || instance.otherAudio == null)
            return;

        instance.otherAudio.Stop();
        switch (sType)
        {
            case SoundType.KNOB:
                instance.otherAudio.PlayOneShot(instance.audioSourcesKnob[instance.currentKnob % instance.audioSourcesKnob.Length]);
                instance.currentKnob++;
                break;
            case SoundType.SWITCH:
                instance.otherAudio.PlayOneShot(instance.audioSourcesSwitch[instance.currentSwitch % instance.audioSourcesSwitch.Length]);
                instance.currentSwitch++;
                break;
            case SoundType.DOOR:
                instance.otherAudio.PlayOneShot(instance.audioSourcesDoor[instance.currentDoor % instance.audioSourcesDoor.Length]);
                instance.currentDoor++;
                break;
            case SoundType.NONE:
                break;
        }
    }

	public void PlaySound(AudioEnum e){

        if (otherAudio == null)
            return;
        
		switch (e) {
            
		case AudioEnum.CorrectClip:
			{
                    if(correctAudio)
                        otherAudio.PlayOneShot (correctAudio, 0.7F);
			}
			break;
		case AudioEnum.clickClip:
			{
                    if(clickAudio)
                        otherAudio.PlayOneShot (clickAudio, 0.7F);
			}
			break;
		case AudioEnum.GameOverClip:
			{
                    if(gameOverAudio) otherAudio.PlayOneShot (gameOverAudio, 0.7F);
			}
			break;
		case AudioEnum.HurryUpClip:
			{
                    if(hurryUpAudio) otherAudio.PlayOneShot (hurryUpAudio, 0.7F);
			}
			break;
		case AudioEnum.ObjectiveAchievedClip:
			{
				otherAudio.PlayOneShot (objectiveAudio, 0.7F);
			}
			break;
		case AudioEnum.ReadyClip:
			{
				otherAudio.PlayOneShot (readyAudio, 0.7F);
			}
			break;
		case AudioEnum.ThemeSong:
			{
                    if(bgMusic && audioBG) audioBG.PlayOneShot (bgMusic, 0.7F);
			}
			break;
		case AudioEnum.WrongClip:
			{
                    if(wrongAudio) otherAudio.PlayOneShot (wrongAudio, 0.7F);
			}
			break;
		case AudioEnum.YouLoseClip:
			{
                    if(gameOverAudio) otherAudio.PlayOneShot (gameOverAudio, 0.7F);
			}
			break;
		case AudioEnum.YouWinClip:
			{
                    if(winAudio)
				        otherAudio.PlayOneShot (winAudio, 0.7F);
			}
			break;
        case AudioEnum.YoureAwesomeClip:
            if (yourAwesomeAudio)
            	otherAudio.PlayOneShot(yourAwesomeAudio, 0.7f);
        	break;
		case AudioEnum.HighScore:
			if(highScoreAudio)
				otherAudio.PlayOneShot(highScoreAudio, 0.7f);
			break;
		}
	}

	public void  StopAudio(){
		otherAudio.Stop();
		audioBG.Stop();
	}
	public void PauseAudio(){
		otherAudio.Pause();
		audioBG.Pause();
	}
	public void UnpauseAudio(){
		otherAudio.UnPause();
		audioBG.UnPause();
	}
}
