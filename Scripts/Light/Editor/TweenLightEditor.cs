﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(TweenLight))]
public class TweenLightEditor : Editor 
{
    bool displayTools = false;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        TweenLight tweener = target as TweenLight;
        if ((displayTools = EditorGUILayout.Foldout(displayTools, "Display Tools")))
        {
            if (Application.isPlaying)
            {
                EditorGUILayout.BeginHorizontal();
                if (GUILayout.Button("PlayForward()"))
                {
                    tweener.PlayForward();
                }
                if (GUILayout.Button("PlayReverse()"))
                {
                    tweener.PlayReverse();
                }
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                if (GUILayout.Button("SkipToEnd()"))
                {
                    tweener.SkipToEnd();
                }
                if (GUILayout.Button("Return()"))
                {
                    tweener.Return();
                }
                EditorGUILayout.EndHorizontal();
            }
            else
            {
                if (GUILayout.Button("Set current as to"))
                {
                    tweener.to = tweener.CurrentValue;
                }

                if (GUILayout.Button("Set current as from"))
                {
                    tweener.from = tweener.CurrentValue;
                }

                if (GUILayout.Button("Assume value of to"))
                {
                    tweener.CurrentValue = tweener.to;
                }

                if (GUILayout.Button("Assume value of from"))
                {
                    tweener.CurrentValue = tweener.from;
                }
            } 
        }
    }
}
