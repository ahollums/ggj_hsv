﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Light)), ExecuteInEditMode]
public class TweenLight : MonoBehaviour 
{
    public float duration = 1f;
    public float from = 0f;
    public float to = 1f;
    public Light m_Light;
    protected bool playingForward = false;

    void Awake()
    {
        m_Light = GetComponent<Light>();
    }

    public void PlayForward()
    {
        playingForward = true;
        StopAllCoroutines();
        StartCoroutine(TweenRoutine(CurrentValue, to));
    }

    public void PlayReverse()
    {
        playingForward = false;
        StopAllCoroutines();
        StartCoroutine(TweenRoutine(CurrentValue, from));
    }

    IEnumerator TweenRoutine(float tween_From, float tween_To)
    {
        playingForward = tween_To == to;
        float distance = Mathf.Abs(CurrentValue - tween_To);
        float direction = (tween_To - tween_From);

        float rate = 0f;
        float timeElapsed = 0f;

        float normalizedDuration = (distance/Mathf.Abs(to - from))*duration;
        float speed = 1/normalizedDuration;
        Debug.Log(normalizedDuration.ToString());
        while(timeElapsed < normalizedDuration)
        {
            CurrentValue = Mathf.Lerp(CurrentValue, tween_From + direction * distance * (timeElapsed / normalizedDuration), rate);
//            CurrentValue = Mathf.Lerp(CurrentValue, tween_To, timeElapsed/normalizedDuration);
            timeElapsed += Time.deltaTime;
            rate += speed * timeElapsed;
            yield return null;
        }
        CurrentValue = tween_To;
    }

    public void SkipToEnd()
    {
        StopAllCoroutines();
        CurrentValue = to;
    }

    public void Return()
    {
        StopAllCoroutines();
        CurrentValue = from;
    }

    public float CurrentValue
    {
        get
        {
            return m_Light ? m_Light.intensity : 0f;
        }
        set
        {
            if (m_Light)
            {
                m_Light.intensity = Mathf.Clamp(value, from, to);
            }
        }
    }
}
