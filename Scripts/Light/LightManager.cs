﻿using UnityEngine;
using System.Collections;

public class LightManager : MonoBehaviour 
{
    static LightManager instance;
    [SerializeField] TweenLight m_Light;

    void Awake()
    {
        instance = this;
    }

    public static void OnLights()
    {
        if (instance == null || instance.m_Light == null)
            return;

        instance.m_Light.SkipToEnd();
    }

    public static void OffLights()
    {
        if (instance == null || instance.m_Light == null)
            return;

        instance.m_Light.Return();
    }

    public static void DimOnLights()
    {
        if (instance == null || instance.m_Light == null)
            return;

        instance.m_Light.PlayForward();
    }

    public static void DimOffLights()
    {
        if (instance == null || instance.m_Light == null)
            return;

        instance.m_Light.PlayReverse();
    }
}
