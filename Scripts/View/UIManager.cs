﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Xml;
using System.Collections.Generic;

[System.Serializable]
public enum OCDColors
{
	GOOD = 0,
	NEUTRAL,
	BAD
}

public class UIManager : MonoBehaviour 
{
    public static UIManager instance;
    public Text centerText;
	public Text dayLabel;
    public Text blackoutDayLabel;
    public Text dailyMessageLabel;
    public Text scoreText;
    public Text countText;
    public Text healthText;

	public UITween leftSwipeTween;
	public UITween downSwipeTween;
	public UITween rightSwipeTween;
	public UITween upSwipeTween;
	public UITween left90Tween;
	public UITween right90Tween;

	public UITween leftDClickTween;
	public UITween leftClickTween;

	public GameObject FailScreen;
	public GameObject VictoryScreen;
    public GameObject TutorialScreen;

    public UITween youreAwesome;
    public UITween damageGraphic;
	public UITween demoModeGraphic;
	public UITween highScoreGraphic;

    public CustomTween mainMenuTween;
    public UITween fader;
    bool inMainMenu = false;

    public Color goodColor;
    public Color badColor;
    public Color neutralColor;

    List<string> dailyMessages = new List<string>();
    public string dailyXml = "DailyMessage";

    [SerializeField] bool logging = false;

    protected virtual void Awake()
    {
        instance = this;
        LoadDailyMessages();
    }

    void LoadDailyMessages()
    {
        XmlNodeList nodeList = XmlTools.GetXmlNodes(dailyXml);
        foreach (XmlNode node in nodeList)
        {
            string dailyMsg = XmlTools.AttributeValue(node, "Text");
            if (!string.IsNullOrEmpty(dailyMsg))
                dailyMessages.Add(dailyMsg);
        }
        dailyMessages.Shuffle();
    }

	public void Start(){
		instance.FailScreen.SetActive (false);
		instance.VictoryScreen.SetActive (false);
	}

    public static void ShowCenterMessage(string text, OCDColors colorChoice, float duration)
    {
        if (instance == null || instance.centerText == null)
            return;

        instance.centerText.text = text;
		instance.centerText.color = GetColor(colorChoice);
        ActionHandle.DelayedAction(duration, () =>
            {
                if(instance.centerText.text != text) return;
                instance.centerText.text = "";
                instance.centerText.color = Color.white;
            });
    }

	static Color GetColor(OCDColors choice)
	{
		Color color = Color.white;
		if(instance)
		{
			switch(choice)
			{
			case OCDColors.GOOD:
				color = instance.goodColor;
				break;
			case OCDColors.NEUTRAL:
				color = instance.neutralColor;
				break;
			case OCDColors.BAD:
				color = instance.badColor;
				break;
			}
		}
			
		return color;
	}

    public static void ShowSwipeIndicator(SwipeDirection direction, bool showMe)
	{
		switch (direction) {
		case SwipeDirection.DOWN:
			if (instance.downSwipeTween)
				instance.downSwipeTween.Show(showMe);
			break;
		case SwipeDirection.LEFT:
			if (instance.leftSwipeTween)
				instance.leftSwipeTween.Show(showMe);
			break;
		case SwipeDirection.RIGHT:
			if (instance.rightSwipeTween)
				instance.rightSwipeTween.Show(showMe);
			break;
		case SwipeDirection.UP:
			if (instance.upSwipeTween)
				instance.upSwipeTween.Show(showMe);
			break;

		}
	}

	public static void ShowTurnIndicators(TurnDirection direction, bool showMe)
	{
		switch (direction) {

		case TurnDirection.LEFT90:
			if (instance.left90Tween)
				instance.left90Tween.Show (showMe);
			break;

		case TurnDirection.RIGHT90:
			if (instance.right90Tween)
				instance.right90Tween.Show (showMe);
			break;
		}
	}

	public static void ShowClickIndicator(ClickMethod clicks, bool showMe){

		switch (clicks) {
		case ClickMethod.DLEFT:

			if (instance.leftDClickTween)
				instance.leftDClickTween.Show(showMe);
			break;
		case ClickMethod.LEFT:

			if (instance.leftClickTween)
				instance.leftClickTween.Show(showMe);
			break;
		}
	}

    public static void ShowFailurePanel(bool show)
    {
		Log ("System failed...");
        if (instance == null || instance.FailScreen == null)
            return;
        
        if (show)
            instance.FailScreen.SetActive(true);

        UITween tweener = instance.FailScreen.GetComponent<UITween>();
        if (tweener)
            tweener.Show(show);
    }

    public static void ShowVictoryPanel(bool show)
    {
		Log("Congratulations, the system was completed.");
        if (instance == null || instance.VictoryScreen == null)
            return;

        if (show)
            instance.VictoryScreen.SetActive(true);
        
        UITween tweener = instance.VictoryScreen.GetComponent<UITween>();
        if (tweener)
            tweener.Show(show);
    }

    public static void ShowMainMenu(bool show)
    {
        if (instance == null || instance.mainMenuTween == null)
            return;
		
		SceneLoader.PauseGame(show);
        instance.inMainMenu = show;
        instance.mainMenuTween.Show(show);
        Fade(show);
    }

    public static void YOUREAWESOME()
    {
        if (instance == null || instance.youreAwesome == null)
            return;

		ShowMainMenu(false);
        instance.youreAwesome.gameObject.SetActive(true);
        instance.youreAwesome.PlayForward();
        instance.youreAwesome.onComplete = () =>
        {
                instance.youreAwesome.gameObject.SetActive(false);
        };
        SoundManger.Play(AudioEnum.YoureAwesomeClip);
    }

	public static void HighScore()
	{
		if(instance == null || instance.highScoreGraphic == null || Player.playedHighScoreClip) return;
		instance.highScoreGraphic.gameObject.SetActive(true);
		instance.highScoreGraphic.PlayForward();
		instance.highScoreGraphic.onComplete = ()=>
		{
			instance.highScoreGraphic.gameObject.SetActive(false);
		};
		SoundManger.Play(AudioEnum.HighScore);
	}

    public static void ToggleMainMenu()
    {
        if (instance == null || instance.mainMenuTween == null)
            return;

        if (instance.inMainMenu)
        {
            ShowMainMenu(false);
        }
        else
            ShowMainMenu(true);
    }

    /// <summary>
    /// Begins the day.
    /// </summary>
    /// <param name="dayNum">Day number.</param>
    /// <param name="duration">Duration.</param>
    public static void BeginDay (int dayNum, float duration)
    {
        if (instance == null || instance.fader == null || instance.blackoutDayLabel == null)
            return;
        instance.fader.gameObject.SetActive(true);
        instance.fader.SkipToEnd();
		instance.blackoutDayLabel.enabled = true;
        instance.blackoutDayLabel.text = dayNum.ToString();
        UITween labelTween = instance.blackoutDayLabel.gameObject.GetComponent<UITween>();
        if (labelTween)
            labelTween.SkipToEnd();

        DailyMessage = instance.dailyMessages[ObjectiveSystem.CurrentDay % instance.dailyMessages.Count];

        DayText = dayNum.ToString();
        ActionHandle.DelayedAction(duration, () =>
            {
                Fade(false);
            });
    }

    /// <summary>
    /// Fade the UIScreen to black if passed true, to clear if false using UITweenAlpha
    /// </summary>
    /// <param name="fadeBlack">If set to <c>true</c> fade black.</param>
    public static void Fade(bool fadeBlack = true)
    {
        if (instance == null || instance.fader == null)
            return;

        instance.fader.Show(fadeBlack);
    }

    /// <summary>
    /// Graphically display this score.
    /// </summary>
    /// <param name="score">Score.</param>
    public static void SetScore(int score)
    {
        if (instance == null || instance.scoreText == null)
            return;

        instance.scoreText.enabled = true;
        instance.scoreText.text = score.ToString();
    }

    public static string CountText
    {
        get
        {
            return instance && instance.countText ? instance.countText.text : null;
        }
        set
        {
            Transform countParent = instance && instance.countText ? instance.countText.transform.parent : null;
            if (countParent)
            {
                if (string.IsNullOrEmpty(value))
                    countParent.gameObject.SetActive(false);
                else
                {
                    instance.countText.text = value;
                    countParent.gameObject.SetActive(true);
                    UITween tweener = countParent.GetComponent<UITween>();
                    if (tweener)
                    {
                        tweener.type = TweenType.PULSE;
                        tweener.PlayForward();
                    }
                }
            }
        }
    }

    public static void ShowTutorial(bool show)
    {
        if (instance == null || instance.TutorialScreen == null)
            return;

        instance.inMainMenu = show;

        instance.TutorialScreen.SetActive(show);
        if (!show)
        {
            if (instance.mainMenuTween)
                instance.mainMenuTween.Return();
            Fade(false);
        }
    }

    public static void ShowDamageTaken(bool show)
    {
        if (instance == null || instance.damageGraphic == null)
            return;

        instance.damageGraphic.gameObject.SetActive(true);
        instance.damageGraphic.Show(show);
        if (!show)
        {
            instance.damageGraphic.onComplete = () =>
            {
                    instance.damageGraphic.gameObject.SetActive(false);
            };
        }
    }

	public static void ShowDemoMode(bool show)
	{
		if(instance == null || instance.demoModeGraphic == null) return;

		instance.demoModeGraphic.gameObject.SetActive(true);
		instance.demoModeGraphic.Show(show);
		if(!show)
		{
			instance.demoModeGraphic.onComplete = ()=>
			{
				instance.demoModeGraphic.gameObject.SetActive(false);
				instance.demoModeGraphic.onComplete = null;
			};
		}
	}

    public static string HitText
    {
        set
        {
            if (instance && instance.healthText)
                instance.healthText.text = value;
        }
    }

    public static string DayText
    {
        set
        {
            if (instance && instance.dayLabel)
                instance.dayLabel.text = value;
        }
    }

    public static string DailyMessage
    {
        set
        {
            if (instance && instance.dailyMessageLabel)
            {
                instance.dailyMessageLabel.text = value;
            }
        }
    }

    public static bool InMainMenu
    {
        get
        {
            return instance ? instance.inMainMenu : false;
        }
    }

    static void Log (string message, LogType type = LogType.Log)
    {
        message = string.Format("[{0}]: {1}", "UIManager", message);
        switch (type)
        {
            case LogType.Log:
                if(instance.logging) Debug.Log(message);
                break;
            case LogType.Warning:
                Debug.LogWarning(message);
                break;
            case LogType.Error:
                Debug.LogError(message);
                break;
        }
    }
}
