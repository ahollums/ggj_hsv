﻿using UnityEngine;
using System.Collections;

public class NextDayButton : SoundButton 
{
    protected override void ButtonClick(GameObject go)
    {
        base.ButtonClick(go);
        ObjectiveSystem.instance.LoadNextDay();
        UIManager.ShowVictoryPanel(false);
    }
}
