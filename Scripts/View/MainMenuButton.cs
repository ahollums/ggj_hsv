﻿using UnityEngine;
using System.Collections;

public class MainMenuButton : SoundButton 
{
    protected override void ButtonClick(GameObject go)
    {
        base.ButtonClick(go);
        UIManager.ToggleMainMenu();
    }
}
