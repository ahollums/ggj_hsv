﻿using UnityEngine;
using System.Collections;

public class TutorialButton : SoundButton 
{
    [System.Serializable]
    public enum ShowType
    {
        SHOW = 0,
        HIDE
    }

    public ShowType type;

    protected override void ButtonClick(GameObject go)
    {
        base.ButtonClick(go);
        switch (type)
        {
            case ShowType.SHOW:
                UIManager.ShowTutorial(true);
                break;
            case ShowType.HIDE:
                UIManager.ShowTutorial(false);
                break;
        }
    }
}
