﻿using UnityEngine;
using System.Collections;

public class ReturnMenuButton : AddOnButton 
{
    protected override void ButtonClick(GameObject go)
    {
        base.ButtonClick(go);
        SceneLoader loader = GameObject.FindObjectOfType<SceneLoader>();
        if (loader)
        {
            loader.LoadStart();
        }
    }
}
