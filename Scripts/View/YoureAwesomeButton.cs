﻿using UnityEngine;
using System.Collections;

public class YoureAwesomeButton : SoundButton 
{
    protected override void ButtonClick(GameObject go)
    {
        base.ButtonClick(go);
		UIManager.ShowMainMenu(false);
		ActionHandle.UnscaledDelayedAction(.5f, ()=>
			{
				UIManager.YOUREAWESOME();
			});
    }
}
