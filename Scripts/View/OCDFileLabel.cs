﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class OCDFileLabel : MonoBehaviour 
{
	Text label;
	public OCDFileSubject subject;

	void Awake()
	{
		label = GetComponent<Text>();
		OCDFileParser fileParser = new OCDFileParser(OCDFileParser.highscoreDoc);
		label.text = fileParser.ReadSubject(subject).ToString();
	}
}
