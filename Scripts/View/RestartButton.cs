﻿using UnityEngine;
using System.Collections;

public class RestartButton : SoundButton 
{
    protected override void ButtonClick(GameObject go)
    {
        base.ButtonClick(go);
        ObjectiveSystem.instance.ResetSystem();
        UIManager.ShowFailurePanel(false);
    }
}
