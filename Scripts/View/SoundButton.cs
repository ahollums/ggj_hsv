﻿using UnityEngine;
using System.Collections;

public class SoundButton : AddOnButton 
{
    public AudioEnum audioClip = AudioEnum.clickClip;

    protected override void ButtonClick(GameObject go)
    {
        base.ButtonClick(go);
//		clickClip = Resources.Load ("clickClip") as AudioClip;
        SoundManger.Play(audioClip);
    }
}
