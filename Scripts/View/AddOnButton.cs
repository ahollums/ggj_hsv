﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class AddOnButton : MonoBehaviour 
{
    protected Button btn;

    protected virtual void Awake()
    {
        if (btn == null)
            btn = GetComponent<Button>();
        RegisterButton(true);
    }

    protected void RegisterButton(bool register)
    {
        if(register)
        {
            RegisterButton(false);
            btn.onClick.AddListener(() =>
                {
                    ButtonClick(gameObject);
                });
        }
        else
        {
            btn.onClick.RemoveListener(() =>
                {
                    ButtonClick(gameObject);
                });
        }
    }

    protected virtual void ButtonClick(GameObject go) {}
}
