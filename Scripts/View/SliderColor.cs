﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

[RequireComponent(typeof(Slider))]
public class SliderColor : MonoBehaviour {
	public Slider slider;
	Image fillImage;
	
	float incrementValues = 0f;

	public List<Color> colorNodes = new List<Color>();
	public bool logging = false;

	// Use this for initialization
	protected virtual void Start () {
		if(slider == null) slider = GetComponent<Slider>();
		incrementValues = (float) (1f/colorNodes.Count);
		if(slider.fillRect != null) fillImage = slider.fillRect.GetComponent<Image>();
		if(slider != null)
		{
			slider.onValueChanged.AddListener(OnSliderValueChanged);
			OnSliderValueChanged(slider.value);
		}
	}

	protected virtual void OnSliderValueChanged(float value)
	{
		if(fillImage != null)
		{
			int incrementCount = (int)(value / incrementValues);
			float actualValue = (value - (incrementCount * incrementValues))/incrementValues;
			if(incrementCount == 0 || incrementCount >= colorNodes.Count) return;
			fillImage.color = Color.Lerp (colorNodes[incrementCount-1],colorNodes[incrementCount], actualValue);
		}
	}

	protected virtual void Log(string message)
	{
		if(logging)
		{
			Debug.Log ("[SliderColor]: "+message);
		}
	}
}
