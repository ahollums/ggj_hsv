﻿using UnityEngine;
using System.Collections;

public class IdleObjectRotation : MonoBehaviour {
	public Vector3 rotationAmount = Vector3.zero;
	public float speed = 0f;
	public bool rotateOnEnable = false;
	public bool returnOnStop = false;
	static Quaternion cacheRotation;
	bool rotating = false;
	
	void Start()
	{
		cacheRotation = transform.rotation;
	}
	
	void Update()
	{
		if(rotating)
		{
			transform.Rotate (rotationAmount*speed);
		}
		if(!rotating && returnOnStop)
		{

			//transform.rotation = Quaternion.Slerp (
		}
	}
	
	public void Rotate()
	{
		rotating = true;
		StopCoroutine (ReturnRoutine ());
	}
	
	public void Stop()
	{
		rotating = false;
		if(returnOnStop)
			Return ();
	}

	public void Toggle()
	{
		rotating = !rotating;
		if(!rotating)
			Stop ();
		else
			Rotate ();
	}
	
	public void Return()
	{
		//transform.rotation = cacheRotation;
		StartCoroutine (ReturnRoutine ());
	}

	IEnumerator ReturnRoutine()
	{
		float rate = 0f;
		while(rate < 1f)
		{
			rate += Time.deltaTime * speed;
			transform.rotation = Quaternion.Slerp(transform.rotation,cacheRotation,rate);
			yield return null;
		}
	}
	
	void OnEnable()
	{
		if(rotateOnEnable)
			Rotate ();

		if(returnOnStop && cacheRotation != transform.rotation)
			transform.rotation = cacheRotation;
	}
}
