﻿using UnityEngine;
using System.Collections;

public class ChallengeObject : InteractiveObject
{
	[System.Serializable]
	public enum ChallengeType
	{
		NONE = -1,
		UD_SWIPE = 0,
		LR_SWIPE,
		TURN,
		CLICK,
	};

    [System.Serializable]
    public enum ObjectType
    {
        NONE = 0,
        LIGHT_TOGGLE,
        LIGHT_DIMMER,
        OFF_LIGHT,
        ON_LIGHT
    }

	public ChallengeType type;
    public ObjectType objType;
	public int phaseToBeUsed;
	public GameObject hotSpot;
	public bool hasAltType;
	public ChallengeType typeAlt = ChallengeType.NONE;
    public SoundType soundType = SoundType.NONE;

    public void GestureHeard(bool positiveMotion)
    {
        SoundManger.PlaySoundEffect(soundType);
        switch (objType)
        {
            case ObjectType.LIGHT_DIMMER:
                if (positiveMotion)
                    LightManager.DimOnLights();
                else
                    LightManager.DimOffLights();
                break;
            case ObjectType.LIGHT_TOGGLE:
                if (positiveMotion)
                    LightManager.OnLights();
                else
                    LightManager.OffLights();
                break;
            case ObjectType.OFF_LIGHT:
                LightManager.OffLights();
                break;
            case ObjectType.ON_LIGHT:
                LightManager.OnLights();
                break;
        }
    }
}
