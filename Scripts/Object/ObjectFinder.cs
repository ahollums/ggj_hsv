﻿using UnityEngine;
using System.Collections;

public class ObjectFinder : MonoBehaviour 
{
    public static ObjectFinder instance;
    public Transform root;
    [SerializeField] bool logging = false;

    void Awake()
    {
        instance = this;
    }

    public static Transform Find(string pName)
    {
        if (instance == null)
            Log("There is no instance of the ObjectFinder in the scene!", LogType.Error);
        else if (instance.root == null)
            Log("There was no root transform to search in!", LogType.Error);
        return instance && instance.root ? instance.root.FindObjectByName(pName) : null;
    }

    public static T FindObjectType<T>() where T : Component
    {
        return GameObject.FindObjectOfType<T>() as T;
    }

    static void Log (string message, LogType type = LogType.Log)
    {
        message = string.Format("[{0}]: {1}", "ObjectFinder", message);
        switch (type)
        {
            case LogType.Log:
                if (instance.logging)
                    Debug.Log(message);
                break;
            case LogType.Warning:
                Debug.LogWarning(message);
                break;
            case LogType.Error:
                Debug.LogError(message);
                break;
        }
    }
}
