﻿using UnityEngine;
using System.Collections;

public class ButtonObject : InteractiveObject {

	public SendMessageEvent[] sendOnClick = new SendMessageEvent[0];
	
	void Start()
	{
		onMouseDown+=LogClick;
	}
	
	protected override void OnMouseDown()
	{
		base.OnMouseDown ();
		
		if(sendOnClick.Length > 0)
			MessageHandle.SendMessage (sendOnClick);
	}

	protected override void OnMouseOver ()
	{
		base.OnMouseOver ();
	}

	protected override void OnMouseExit ()
	{
		base.OnMouseExit ();
	}

	void LogClick()
	{
		Log (transform.name +" clicked.");
	}
}
