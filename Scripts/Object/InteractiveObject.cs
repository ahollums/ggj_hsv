using UnityEngine;
using System.Collections;
using System;

[System.Serializable]
public enum InteractType
{
	NONE = 0,
	HOVER,
	CLICK,
	DRAG
}

[RequireComponent(typeof(Collider))]
public class InteractiveObject : MonoBehaviour 
{
	protected bool dragging = false;
	protected bool clicked = false;
	protected bool hovering = false;
	protected Vector3 clickedPos = Vector2.zero;
	
	public delegate void OnDragStartDelegate();
	public delegate void OnDragDelegate();
	public delegate void OnDragReleaseDelegate();
	public delegate void OnMouseOverDelegate();
	public delegate void OnMouseExitDelegate();
	public delegate void OnMouseDownDelegate();
	public delegate void OnMouseUpDelegate();
	public OnDragStartDelegate onDragStart;
	public OnDragDelegate onDrag;
	public OnDragReleaseDelegate onDragRelease;
	public OnMouseOverDelegate onMouseOver;
	public OnMouseExitDelegate onMouseExit;
	public OnMouseDownDelegate onMouseDown;
	public OnMouseUpDelegate onMouseUp;

	public bool logging = false;
	
	void Start()
	{

	}
	
	protected virtual void OnMouseOver()
	{
		if(hovering)
			return;

		if(onMouseOver!=null) onMouseOver();
		hovering = true;
	}

	protected virtual void OnMouseExit()
	{
		hovering = false;
		if(onMouseExit != null)
			onMouseExit();
	}
	
	protected virtual void OnMouseDown()
	{
		clicked = true;
		clickedPos = Input.mousePosition;
		
		if(onMouseDown!=null) onMouseDown();
	}

	protected virtual void OnMouseUp()
	{
		clicked = false;
		
		if(onMouseUp!=null) onMouseUp();

		if(dragging)
			OnDragRelease ();
	}
	
	protected virtual void OnDragStart()
	{
		if(onDragStart!=null) onDragStart();
	}
	
	protected virtual void OnDragRelease()
	{
		if(onDragRelease!=null) onDragRelease();
	}
	
	protected virtual void OnDrag()
	{
		if(onDrag!=null) onDrag();
	}

	public virtual void OnDestoryComponent()
	{
		Destroy(this);
	}

	protected virtual void Log(string message)
	{
		if(logging)
			Debug.Log ("[InteractiveObject]: "+message);
	}
}

