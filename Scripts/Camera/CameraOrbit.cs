﻿using UnityEngine;
using System.Collections;

public class CameraOrbit : MonoBehaviour {
	public Transform target;
	public float orbitSpeed = 50f;
	public Vector3 radius = new Vector3(5f,0f,0f);
	public Vector3 offset = new Vector3(0f,1.5f,0f);
	public bool orbiting = false;
	public bool orbitOnStart = false;

	public float lerpTime = 100f;
	public bool lerpCamToOrbitStart = false;

	public bool logging = false;
	// Use this for initialization
	void Start () {
		if(orbitOnStart)
			Orbit ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Orbit()
	{
		orbiting = true;
		if(lerpCamToOrbitStart)
		{
			StartCoroutine ("LerpToStart");
		}
		else
		{
			StartCoroutine ("Engine");
		}
	}

	IEnumerator LerpToStart()
	{
		GameObject lerpTarget = new GameObject("lerpTarget");
		float startRotation = Time.deltaTime*orbitSpeed;
		lerpTarget.transform.rotation = Quaternion.Euler (new Vector3(0,-startRotation,0));
		lerpTarget.transform.position = target.transform.position + lerpTarget.transform.rotation * radius;
		lerpTarget.transform.position += offset;
		lerpTarget.transform.LookAt (target);
		float rate = 0f;
		while(!LerpComplete (transform,lerpTarget.transform))
		{
			rate += Time.deltaTime/lerpTime;
			transform.position = Vector3.Lerp (transform.position,lerpTarget.transform.position,rate);
			transform.rotation = Quaternion.Slerp (transform.rotation,lerpTarget.transform.rotation,rate);
			yield return null;
		}
		yield return null;
		StartCoroutine ("Engine");
	}

	public static bool LerpComplete(Transform from, Transform to)
	{
		return LerpPositionComplete (from.position, to.position) && LerpPositionComplete(from.rotation.eulerAngles, to.rotation.eulerAngles);
	}
	
	static bool LerpPositionComplete(Vector3 from, Vector3 to)
	{
		bool xIn = Mathf.Abs (to.x - from.x) < .1f;
		bool yIn = Mathf.Abs (to.y - from.y) < .1f;
		bool zIn = Mathf.Abs (to.z - from.z) < .1f;
		bool complete = xIn && yIn && zIn;
		return complete;
	}

	IEnumerator Engine()
	{
		// Rotate on y axis - increment y rotation
		float currentRotation = 0f;
		while(orbiting)
		{
			currentRotation += Time.deltaTime*orbitSpeed;
			// Set new rotation based on zero vector rotation added to the incremented y rotation
			transform.rotation = Quaternion.Euler (new Vector3(0, -currentRotation, 0));
			AdjustPosition (radius);
			FaceTarget ();

			yield return null;
		}
		yield return null;
	}

	/* <summary>
	 * Adjusts the camera so its z-axis is pointing at the target.
	 * </summary>
	 */
	void FaceTarget()
	{
		transform.LookAt(target);
	}

	/* <summary>
	 * Move the object the radius distance away from the target's position
	 * Then apply offset values
	 * At this point the camera is looking head on at the object.
	 * </summary>
	 */
	void AdjustPosition(Vector3 radius)
	{
		transform.position = target.transform.position + transform.rotation * radius;
		transform.position += offset;
	}

	public void Stop()
	{
		orbiting = false;
	}

	void Log(string message)
	{
		if(logging)
		{
			Debug.Log ("[CameraOrbit]: "+message);
		}

	}
}
