﻿using UnityEngine;
using System.Collections;

public class CamController : MonoBehaviour {

    public static CamController instance;
	public float speed;
    public Transform hotspotRoot;
	Vector3 startPosition;
	Quaternion startRotation;
    [SerializeField] bool logging = false;

    void Awake()
    {
        instance = this;    
    }

	void Start(){
		startPosition = transform.position;
		startRotation = transform.rotation;
		//		RotateCamera (new Vector3 (200, 0, 0));
	}

	void Update(){

//		transform.position = Vector3.Lerp (transform.position,new Vector3 (200, 0, 0), speed * Time.deltaTime);
//		Quaternion newRot = Quaternion.Euler (new Vector3 (100, 0, 0)); // get the equivalent quaternion
//		transform.rotation = Quaternion.Slerp (transform.rotation, newRot, speed * Time.deltaTime);
	}
	public void RotateCamera(Vector3 rotateTo){
		StartCoroutine ("_RotateCamera",rotateTo);
	}
	public void MoveCamera(Vector3 moveTo){
		StartCoroutine ("_MoveCamera",moveTo);
	}

	public static void Reset()
	{
		instance.StopAllCoroutines();
		instance.RotateCamera(instance.startRotation.eulerAngles);
		instance.MoveCamera(instance.startPosition);
	}

    public void SetTarget(Transform target)
    {
        if (target == null)
            return;
        StopAllCoroutines();
        RotateCamera(target.eulerAngles);
        MoveCamera(target.position);
    }

    public static void SetTarget(string pName)
    {
        if (instance.hotspotRoot == null)
        {
            instance.Log("There was no hotspotRoot attached to the CamController");
            return;
        }

        Transform t = instance.hotspotRoot.FindObjectByName(pName);
        if (t == null)
        {
            instance.Log(string.Format("Could not find a hotspot by name [{0}]", pName), LogType.Warning);
            return;
        }
     
        instance.SetTarget(t);
    }

	IEnumerator _MoveCamera( Vector3 cameraPos){
		while (true) {
			transform.position = Vector3.Lerp (transform.position, cameraPos, speed * Time.deltaTime);
			yield return null;
		}

	}
	IEnumerator _RotateCamera(Vector3 cameraAngles){

		while (true) {
			Quaternion newRot = Quaternion.Euler (cameraAngles); // get the equivalent quaternion
			transform.rotation = Quaternion.Slerp (transform.rotation, newRot, speed * Time.deltaTime);
			yield return null;
		}
	}

    void Log (string message, LogType type = LogType.Log)
    {
        message = string.Format("[{0}]: {1}", "CamController", message);
        switch (type)
        {
            case LogType.Log:
                if(logging) Debug.Log(message);
                break;
            case LogType.Warning:
                Debug.LogWarning(message);
                break;
            case LogType.Error:
                Debug.LogError(message);
                break;
        }
    }

}
