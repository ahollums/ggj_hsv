﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public enum smeOption
{
	NONE=0,
	ENABLE,
	DISABLE
}

[System.Serializable]
public struct SendMessageEvent
{
	public Transform receiver;
	public string method;
	public string param;
	public smeOption option;
	
	public override bool Equals (object obj)
	{
		if(obj == null && receiver == null)
		{
			MessageHandle.Log("NULL SME");
			return true;
		}
		else
			return base.Equals (obj);
	}

	public override int GetHashCode ()
	{
		return base.GetHashCode ();
	}
	
	public override string ToString ()
	{
		if(receiver != null && method != null)
			return string.Format ("[SendMessageEvent]: -receiver ["+receiver.name+"] -method ["+method+"]"+(param == null ? "-param "+ param : ""));
		else
			return string.Format ("[SendMessageEvent]: INVALID");
	}

	public string GetUndoMethod()
	{
		if(method.ToLower() == "enable")
			return "Disable";

		if(method.ToLower () == "disable")
			return "Enable";

		if(method.ToLower() == "playforward")
			return "PlayReverse";

		if(method.ToLower () == "playreverse")
			return "PlayForward";

		return "UndoMessageEvent";
	}
}

public class MessageHandle : MonoBehaviour 
{
	private static MessageHandle instance;

	public bool logging = false;
	
	void Start()
	{
		instance = this;
	}
	
	public static void SendMessage(SendMessageEvent sme)
	{
		if(instance==null) SelfInstantiate();
		Log ("SendMessage() - "+sme.ToString ());
		if(!sme.Equals(null))
		{
			if(sme.param == null || sme.param == "")
				sme.receiver.SendMessage (sme.method,SendMessageOptions.DontRequireReceiver);
			else
				sme.receiver.SendMessage (sme.method,sme.param,SendMessageOptions.DontRequireReceiver);

			if(sme.option == smeOption.ENABLE)
				sme.receiver.gameObject.SetActive (true);
			else if(sme.option == smeOption.DISABLE)
				sme.receiver.gameObject.SetActive (false);
		}
	}
	
	public static void SendMessage(SendMessageEvent[] smes)
	{
		if(instance==null) SelfInstantiate();
		if(smes.Length > 0)
		{
			foreach(SendMessageEvent sme in smes)
			{
				if(!sme.Equals (null))
					SendMessage (sme);
			}
		}
	}

	public static void SendMessage(List<SendMessageEvent> smes)
	{
		if(instance==null) SelfInstantiate();
		if(smes == null) return;
		if(smes.Count > 0)
		{
			foreach(SendMessageEvent sme in smes)
			{
				if(!sme.Equals (null))
					SendMessage (sme);
			}
		}
	}
	
	public static void SendUndo(SendMessageEvent sme)
	{
		if(instance==null) SelfInstantiate();
		Log ("SendUndo() - "+sme.ToString());
		if(!sme.Equals(null))
		{
			if(sme.param == null || sme.param == "")
				sme.receiver.SendMessage (sme.GetUndoMethod(),SendMessageOptions.DontRequireReceiver);
			else
				sme.receiver.SendMessage (sme.GetUndoMethod (),sme.param,SendMessageOptions.DontRequireReceiver);

			if(sme.option == smeOption.ENABLE)
				sme.receiver.gameObject.SetActive (false);
			else if(sme.option == smeOption.DISABLE)
				sme.receiver.gameObject.SetActive (true);
		}
	}
	
	public static void SendUndo(SendMessageEvent[] smes)
	{
		if(instance==null) SelfInstantiate();
		if(smes.Length > 0)
		{
			foreach(SendMessageEvent sme in smes)
			{
				if(!sme.Equals (null))
					SendUndo (sme);
			}
		}
	}
	
	static void SelfInstantiate()
	{
		GameObject self = new GameObject();
		self.name = "MessageHandle";
		MessageHandle handle = self.AddComponent<MessageHandle>();
		handle.Start ();
	}
	
	public static void Log(string message)
	{
		if(instance==null) SelfInstantiate();
		if(instance.logging)
		{
			Debug.Log ("[MessageHandle]: "+message);
		}
	}
}
