﻿using UnityEngine;
using System.Collections;
using System.Reflection;
using System;
using System.Collections.Generic;

public static class ExtensionClass
{
    public static bool InRange<T>(this IList<T> list, int index)
    {
        return index >= 0 && index < list.Count;
    }

    public static void Shuffle<T>(this IList<T> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = UnityEngine.Random.Range(0, n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }

    public static T AddMissingComponent<T>(this GameObject go) where T : Component
    {
        return go.GetComponent<T>() ? go.GetComponent<T>() : go.AddComponent<T>();
    }

    public static Transform FindObjectByName(this Transform root, string objectName)
    {
        if (!string.IsNullOrEmpty(objectName))
        {
            if (root.name.Equals(objectName))
            {
                return root;
            }
        }

        Transform handle = root.Find(objectName);
        if (handle != null)
            return handle;

        handle = null;
        foreach (Transform child in root)
        {
            handle = child.FindObjectByName(objectName);
            if (handle != null)
                return handle;
        }

        return null;
    }

    public static void Show (this UITween tweener, bool show)
    {
        if (tweener == null)
            return;

        if (show)
            tweener.PlayForward();
        else
            tweener.PlayReverse();
    }

    public static void Show (this CustomTween tweener, bool show)
    {
        if (tweener == null)
            return;

        if (show)
            tweener.PlayForward();
        else
            tweener.PlayReverse();
    }

    public static void PlayTween(this CustomTween tweener, SwipeDirection direction)
    {
        if (tweener == null)
            return;

        switch (direction)
        {
            case SwipeDirection.LEFT:
                tweener.PlayReverse();
                break;
            case SwipeDirection.RIGHT:
                tweener.PlayForward();
                break;
            case SwipeDirection.UP:
                tweener.PlayForward();
                break;
            case SwipeDirection.DOWN:
                tweener.PlayReverse();
                break;
        }
    }

    public static void PlayTween(this CustomTween tweener, TurnDirection direction)
    {
        if (tweener == null)
            return;

        switch (direction)
        {
            case TurnDirection.LEFT90:
                tweener.PlayReverse();
                break;
            case TurnDirection.LEFT180:
                tweener.PlayReverse();
                break;
            case TurnDirection.RIGHT90:
                tweener.PlayForward();
                break;
            case TurnDirection.RIGHT180:
                tweener.PlayForward();
                break;
        }
    }

    public static void PlayTween(this CustomTween tweener, ClickMethod method)
    {
        if (tweener == null)
            return;

        switch (method)
        {
            case ClickMethod.DLEFT:
                tweener.PlayForward();
                break;
            case ClickMethod.DRIGHT:
                tweener.PlayReverse();
                break;
            case ClickMethod.LEFT:
                tweener.PlayForward();
                break;
            case ClickMethod.RIGHT:
                tweener.PlayReverse();
                break;
        }
    }

    public static void SetAlpha(this Color c, float alpha)
    {
        c = new Color(c.r, c.g, c.b, alpha);
    }
}
