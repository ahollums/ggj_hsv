﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

public class SequenceGenerator : MonoBehaviour
{
	public static SequenceGenerator instance;
	public List<ChallengeObject> challengeObjects = new List<ChallengeObject>();
	public int difficulty;
	private int numObjectives = 2;
	private int numChallenges = 2;

	// Use this for initialization
	void Awake()
	{
		instance = this;
	}

	public static string GenerateXml(int phase)
	{
		if (phase == 1) {
			if (instance.numObjectives > 2) {
				instance.numObjectives = 2;
				instance.numChallenges = 2;
			}
		}

		XmlDocument xml = new XmlDocument();
		XmlNode rootNode = xml.CreateElement("Objectives");
		xml.AppendChild(rootNode);

		List<ChallengeObject> tmp = new List<ChallengeObject>(instance.challengeObjects.ToArray());
		instance.challengeObjects.ForEach(x => { if (x.phaseToBeUsed > phase) { tmp.Remove(x); } });
		instance.numObjectives = (phase % 2) == 0 ? instance.numObjectives + 1 : instance.numObjectives;
		instance.numChallenges = (phase % 2) == 0 ? instance.numChallenges + 1 : instance.numChallenges;

		ShuffleChallengePositions();

		for (int i = 0; i < instance.numObjectives; i++)
		{
			XmlNode obj = xml.CreateElement("Objective");
			XmlAttribute attrObjType = xml.CreateAttribute("Type");
			attrObjType.Value = "Default";
			XmlAttribute attrObjId = xml.CreateAttribute("ID");
			attrObjId.Value = (i + 1).ToString();
			obj.Attributes.Append(attrObjType);
			obj.Attributes.Append(attrObjId);

			for (int j = 0; j < instance.numChallenges; j++)
			{
				ChallengeObject chalObj = tmp[Random.Range(0, tmp.Count - 1)];
				XmlNode chal = xml.CreateElement("Challenge");
				XmlAttribute attrChalType = xml.CreateAttribute("Type");
				XmlAttribute attrChalDir = xml.CreateAttribute("Direction");
				ChallengeObject.ChallengeType chalType = chalObj.type;
				if (chalObj.hasAltType)
				{
					if (Random.Range(-1f, 1f) < 0)
					{
						chalType = chalObj.type;
					}
					else {
						chalType = chalObj.typeAlt;
					}
				}

				switch (chalType)
				{
					case ChallengeObject.ChallengeType.CLICK:
						attrChalType.Value = "Click";
						attrChalDir.Value = Random.Range(-1f, 1f) < 0 ? "Left" : "DLeft";
						break;
					case ChallengeObject.ChallengeType.TURN:
						attrChalType.Value = "Turn";
						attrChalDir.Value = Random.Range(-1f, 1f) < 0 ? "Left90" : "Right90";
						break;
					case ChallengeObject.ChallengeType.UD_SWIPE:
						attrChalType.Value = "Swipe";
						attrChalDir.Value = Random.Range(-1f, 1f) < 0 ? "Up" : "Down";
						break;
					case ChallengeObject.ChallengeType.LR_SWIPE:
					default:
						attrChalType.Value = "Swipe";
						attrChalDir.Value = Random.Range(-1f, 1f) < 0 ? "Left" : "Right";
						break;
				}

				XmlAttribute attrChalId = xml.CreateAttribute("ID");
				attrChalId.Value = (i + 1).ToString("D3");
				XmlAttribute attrChalFocus = xml.CreateAttribute("Focus");
				attrChalFocus.Value = chalObj.name;
				XmlAttribute attrChalHs = xml.CreateAttribute("Hotspot");
				attrChalHs.Value = chalObj.hotSpot.name;

				chal.Attributes.Append(attrChalType);
				chal.Attributes.Append(attrChalId);
				chal.Attributes.Append(attrChalDir);
				chal.Attributes.Append(attrChalFocus);
				chal.Attributes.Append(attrChalHs);

				obj.AppendChild(chal);
			}

			rootNode.AppendChild(obj);
		}

		return xml.OuterXml;
	}

	public static void ShuffleChallengePositions()
	{
		List<ChallengeObject> tmp = instance.challengeObjects.GetRange(4, 4);
		List<KeyValuePair<Vector3, GameObject>> tmpValues = new List<KeyValuePair<Vector3, GameObject>>();
		tmp.Shuffle();
		tmp.ForEach(t => { tmpValues.Add(new KeyValuePair<Vector3, GameObject>(t.transform.parent.transform.position, t.hotSpot)); });

		for (int i = 0; i < tmpValues.Count; i++) {
			instance.challengeObjects[i + 4].transform.parent.transform.position = tmpValues[i].Key;
			instance.challengeObjects[i + 4].hotSpot = tmpValues[i].Value;
		}

		instance.challengeObjects[instance.challengeObjects.Count - 1].transform.parent.transform.position = tmpValues[3].Key;
		instance.challengeObjects[instance.challengeObjects.Count - 1].hotSpot = tmpValues[3].Value;
	}
}
