﻿using UnityEngine;
using System.Collections;
using System.Xml;

public class XmlTools : MonoBehaviour 
{
    public string xmlResource = "Objectives.xml";
	
    public void TryLoadXml()
    {
        XmlDocument xmlDoc = new XmlDocument();
        if (LoadXml(xmlResource, out xmlDoc))
            Log(string.Format("The XML Resource [{0}] successfully loaded!", xmlResource));
    }

    public static bool LoadXml(string pXmlPath, out XmlDocument pXmlDoc)
    {
        pXmlDoc = new XmlDocument();
        TextAsset ta = Resources.Load <TextAsset>(pXmlPath);
        if (!ta)
        {
            Log(string.Format("Text asset [{0}] failed to load!", pXmlPath), LogType.Error);
            return false;
        }

        string fileString = ta.ToString();
        try
        {
            pXmlDoc.LoadXml(fileString);
            return true;
        }catch (System.Exception error)
        {
            Log(string.Format("Loading file [{0}] threw an exception: {1}", pXmlPath, error.ToString()), LogType.Error);
            return false;
        }
    }

    public static XmlNodeList GetXmlNodes(string pXmlPath)
    {
        XmlDocument doc = new XmlDocument();
        if (!LoadXml(pXmlPath, out doc))
            return null;
        XmlNode root = doc.DocumentElement;
        return root.ChildNodes;
    }

    public static XmlNode GetRootNode(string pXmlPath)
    {
        XmlDocument doc = new XmlDocument();
        if (!LoadXml(pXmlPath, out doc))
            return null;
        return doc.DocumentElement;
    }

    public static string AttributeValue(XmlNode pNode, string text)
    {
        return !string.IsNullOrEmpty(text) && pNode != null && pNode.Attributes[text]
        != null && pNode.Attributes[text].Value != null ?
            pNode.Attributes[text].Value : null;
    }

    protected static void Log (string message, LogType type = LogType.Log)
    {
        message = string.Format("[{0}]: {1}", "XmlTools", message);
        switch (type)
        {
            case LogType.Log:
                Debug.Log(message);
                break;
            case LogType.Warning:
                Debug.LogWarning(message);
                break;
            case LogType.Error:
                Debug.LogError(message);
                break;
        }
    }
}
