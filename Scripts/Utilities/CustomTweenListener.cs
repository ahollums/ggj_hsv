﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

[RequireComponent(typeof(CustomTween))]
public class CustomTweenListener : MonoBehaviour 
{
	protected CustomTween tweener;
	public UnityEvent onForward;
	public UnityEvent onReverse;
	public UnityEvent onForwardComplete;
	public UnityEvent onReverseComplete;
	public bool logging = false;

	protected virtual void Awake()
	{
		tweener = GetComponent<CustomTween>();
		RegisterTween(true);
	}

	protected void RegisterTween(bool register)
	{
		if(tweener == null) return;

		if(register)
		{
			RegisterTween(false);
			tweener.onForward+=OnForward;
			tweener.onReverse+=OnReverse;
			tweener.onForwardComplete+=OnForwardComplete;
			tweener.onReverseComplete+=OnReverseComplete;
		}
		else
		{
			tweener.onForward-=OnForward;
			tweener.onReverse-=OnReverse;
			tweener.onForwardComplete-=OnForwardComplete;
			tweener.onReverseComplete-=OnReverseComplete;
		}
	}

	protected virtual void OnReverseComplete()
	{
		Log ("Reverse completed!");
		if(onReverseComplete != null) onReverseComplete.Invoke();
	}

	protected virtual void OnForwardComplete()
	{
		Log ("Forward completed!");
		if(onForwardComplete != null) onForwardComplete.Invoke();
	}

	protected virtual void OnForward()
	{
		Log ("Forward called!");
		if(onForward != null) onForward.Invoke();
	}

	protected virtual void OnReverse()
	{
		Log ("Reverse called!");
		if(onReverse != null) onReverse.Invoke();
	}

	protected void OnDestroy()
	{
		RegisterTween(false);
	}

	protected void Log(string message, LogType type = LogType.Log)
	{
		message = string.Format("[{0}]: ", GetType().Name) + message;
		switch(type)
		{
		case LogType.Log:
			if(logging) Debug.Log(message);
			break;
		case LogType.Warning:
			Debug.LogWarning(message);
			break;
		case LogType.Error:
			Debug.LogError(message);
			break;
		}
	}
}
