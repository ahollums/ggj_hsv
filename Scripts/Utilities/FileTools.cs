using UnityEngine;
using System.Collections;
using System;
using System.IO;

[ExecuteInEditMode]
public class FileTools : MonoBehaviour 
{
	public string fileName;

	// Use external for files that need be updated and altered at runtime in the builds, such as save files / logs
	// Use internal for files that are just used in the development process.
	// Use resources for files that are to be read from but not altered at runtime, such as data lists, etc.
	public enum FilePathChoice
	{
		EXTERNAL = 0, // Searches for files in the system data folder for this application, i.e. "C:/Users/jdoe/AppData/LocalLow/Camber/simis/file.txt"
		INTERNAL, // Searches for files starting from "Assets/"
	}

	/// <summary>
	/// Writes to the file path( with respect to the external data pathing for the application.
	/// </summary>
	/// <returns><c>true</c>, if to was writed, <c>false</c> otherwise.</returns>
	/// <param name="pFileName">P file name.</param>
	/// <param name="pText">P text.</param>
	/// <param name="append">If set to <c>true</c> append.</param>
	public static bool WriteTo (string pFileName, string pText, bool append = true)
	{
		string fullPath = Path.Combine(ExternalDataPath, pFileName);
		if(CreateFile (fullPath)) Log (string.Format ("The file at [{0}] did not exist, creating it now.", fullPath));
		try
		{
			StreamWriter sw = new StreamWriter(fullPath, append);
			Log (string.Format ("Writing content [{0}] to the file at [{1}]...", pText, fullPath)); 
			sw.WriteLine (pText);
			sw.Flush ();
			sw.Close ();
			return true;
		}catch (Exception e)
		{
			Log ("Couldn't write to the file! "+e.ToString (), LogType.Error);
			return false;
		}
	}

	/// <summary>
	/// Writes to the file path with respect to the data path specified.
	/// </summary>
	/// <returns><c>true</c>, if to was writed, <c>false</c> otherwise.</returns>
	/// <param name="pChoice">P choice.</param>
	/// <param name="pFileName">P file name.</param>
	/// <param name="pText">P text.</param>
	/// <param name="append">If set to <c>true</c> append.</param>
	public static bool WriteTo(FilePathChoice pChoice, string pFileName, string pText, bool append = true)
	{
		string dataPath = "";

		switch(pChoice)
		{
		case FilePathChoice.INTERNAL:
			dataPath = InternalDataPath;
			break;
		case FilePathChoice.EXTERNAL:
			dataPath = ExternalDataPath;
			break;
		}

		string fullPath = Path.Combine (dataPath, pFileName);
		if(CreateFile (fullPath)) Log (string.Format ("The file at [{0}] did not exist, creating it now.", fullPath));
		try
		{
			StreamWriter sw = new StreamWriter(fullPath, append);
			Log (string.Format ("Writing content [{0}] to the file at [{1}]...", pText, fullPath)); 
			sw.WriteLine (pText);
			sw.Flush ();
			sw.Close ();
			return true;
		}catch (Exception e)
		{
			Log ("Couldn't write to the file! "+e.ToString (), LogType.Error);
			return false;
		}
	}

	public static bool ClearTextFile(string pFileName)
	{
		string fullPath = Path.Combine (ExternalDataPath, pFileName);
		if(CreateFile (fullPath)) Log (string.Format ("The file at [{0}] did not exist, creating it now.", fullPath));
		try
		{
			StreamWriter sw = new StreamWriter(fullPath);
			sw.Write ("");
			sw.Flush ();
			sw.Close ();
			return true;
		}catch (Exception e)
		{
			Log ("Couldn't write to the file! "+e.ToString (), LogType.Error);
			return false;
		}
	}

	public static string ReadFrom (string pFileName)
	{
		string fullPath = Path.Combine (ExternalDataPath, pFileName);
		string output = "";
		try
		{
			if(File.Exists (fullPath))
			{
				StreamReader sr = new StreamReader (fullPath);
				while(!sr.EndOfStream)
				{
					output += sr.ReadLine () + "\n";
				}
				sr.Close ();
			}
			return output.TrimEnd ();
		}catch(Exception e)
		{
			Log ("Couldn't read the file! "+ e.ToString (), LogType.Error);
			return null;
		}
	}

	public static string ReadFromResources (string pFilePath)
	{
		TextAsset ta = Resources.Load (pFilePath) as TextAsset;
		if(ta == null)
		{
			Log (string.Format ("There were no files found by the path [{0}] in any resource folders.", pFilePath), LogType.Error);
			return null;
		}
		else return ta.text;
	}

	public static bool CreateFile (string pFileName)
	{
		if(!File.Exists (pFileName))
		{
			try
			{
				File.Create (pFileName).Dispose ();
			}catch(DirectoryNotFoundException e)
			{
				Log ("Directory not found! "+e.ToString (), LogType.Error);
				return false;
			}
			return true;
		}
		return false;
	}

	public static string ExternalDataPath
	{
		get
		{
			return Application.persistentDataPath;
		}
	}

	public static string InternalDataPath
	{
		get
		{
			return Application.dataPath;
		}
	}

	static void Log(string message, LogType type = LogType.Log)
	{
		message = "[FileTools]: "+ message;
		switch(type)
		{
		case LogType.Error:
			Debug.LogError (message);
			break;
		case LogType.Warning:
			Debug.LogWarning (message);
			break;
		default:
			Debug.Log (message);
			break;
		}
	}
}
