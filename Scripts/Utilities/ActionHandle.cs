﻿using UnityEngine;
using System.Collections;
using System;

public class ActionHandle : MonoBehaviour {
	private static ActionHandle instance;
	
	public void Start()
	{
		instance = this;
	}
	
	public static void DelayedAction(float delay, Action action)
	{
		if(instance == null) SelfInstantiate();
		instance.StartCoroutine(DelayedActionRoutine (delay,action));
	}

	public static void UnscaledDelayedAction(float delay, Action action)
	{
		if(instance == null) SelfInstantiate();
		instance.StartCoroutine(UnscaledDelayedActionRoutine (delay,action));
	}
	
	public static void DelayedAction(float delay, Action[] actions)
	{
		if(instance == null) SelfInstantiate();
		instance.StartCoroutine(DelayedActionRoutine(delay,actions));
	}
	
	static IEnumerator DelayedActionRoutine(float delay, Action action)
	{
		yield return new WaitForSeconds(delay);
		if(action != null)
			action();
	}

	static IEnumerator UnscaledDelayedActionRoutine(float delay, Action action)
	{
		float timeElapsed = 0f;
		while(timeElapsed < delay)
		{
			timeElapsed += Time.unscaledDeltaTime;
			yield return null;
		}

		if(action != null)
			action();
	}

	static IEnumerator DelayedActionRoutine(float delay, Action[] actions)
	{
		yield return new WaitForSeconds(delay);
		if(actions.Length > 0)
		{
			foreach(Action action in actions)
			{
				if(action!=null)
					action();
			}
		}	
	}

	public static void WaitForFrames(int frames, Action action)
	{
		if(instance == null) SelfInstantiate();
		instance.StartCoroutine(WaitForFramesRoutine(frames, action));
	}

	public static void WaitForFrames(int frames, Action[] actions)
	{
		if(instance == null) SelfInstantiate();
		instance.StartCoroutine (WaitForFramesRoutine (frames,actions));
	}

	static IEnumerator WaitForFramesRoutine(int frames, Action action)
	{
		for(int i=0;i<frames;i++)
		{
			yield return new WaitForEndOfFrame();
		}
		if(action!=null) action();
	}

	static IEnumerator WaitForFramesRoutine(int frames, Action[] actions)
	{
		for(int i=0;i<frames;i++)
		{
			yield return new WaitForEndOfFrame();
		}
		foreach(Action action in actions)
			if(action!=null) action();
	}

	static void SelfInstantiate()
	{
		GameObject self = new GameObject();
		self.name = "ActionHandle";
		ActionHandle handle = self.AddComponent<ActionHandle>();
		handle.Start ();
	}
}
