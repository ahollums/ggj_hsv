﻿using UnityEngine;
using System.Collections;

public class InspectorNote : MonoBehaviour {
	public bool editMode = true;
	public string note = "Type notes here.\nSave when finished.";

	public void ToggleLock()
	{
		editMode = !editMode;
	}

	void Start()
	{
		this.enabled = false;
	}
}
