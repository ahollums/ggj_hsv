﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(XmlTools))]
public class XmlToolsEditor : Editor 
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        XmlTools tools = target as XmlTools;
        if (GUILayout.Button("LoadXML"))
            tools.TryLoadXml();
    }
}
