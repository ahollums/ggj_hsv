﻿using UnityEngine;
using System.Collections;

public class RandomColors : MonoBehaviour {
	
	public float duration = 1.0F;
	public Color colorStart;
	public Color colorEnd;
	public Light lt;
	public float i;

	void Start() {
		
		lt = GetComponent<Light>();
		colorStart = new Color(Random.value, Random.value, Random.value);
		colorEnd = new Color(Random.value, Random.value, Random.value);
	}
	void Update() {
		
//		float t = Mathf.PingPong(Time.time, duration) / duration;

		i += Time.deltaTime * duration;
		lt.color = Color.Lerp(colorStart, colorEnd, i);
		if(i >= 1) {
			i = 0;
			colorStart = lt.color;
			colorEnd = new Color(Random.value, Random.value, Random.value);
		}

	}
}