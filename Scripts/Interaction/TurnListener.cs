﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public enum TurnDirection
{
	LEFT90 = 0,
	RIGHT90,
	LEFT180,
	RIGHT180,
}

public class TurnListener : MonoBehaviour
{
	public static TurnListener instance;

	public delegate void TurnDelegate(TurnDirection t);
	public static TurnDelegate onTurn;

	[SerializeField]
	protected float m_checkRate;
	protected float m_mouseDownTime;

	protected Vector3 m_firstPos;
	protected Vector3 m_currentPos;
	protected float m_currentAngle;

	protected LineRenderer lineRenderer;
	public static bool canDrawLine = false;
	protected bool drawLine = false;
	protected Vector3 firstPos = Vector3.one * float.MaxValue;
	protected Vector3 lastPos = Vector3.one * float.MaxValue;
	protected List<Vector3> linePoints = new List<Vector3>();
	[SerializeField]
	protected float threshold = 0.001f;
	protected int lineCount = 0;

	protected virtual void Awake()
	{
		instance = this;
		lineRenderer = gameObject.AddMissingComponent<LineRenderer>();
	}

	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	protected void Update()
	{
		if (onTurn == null) {
			// Reset everything
			if (drawLine) {
				lineRenderer.SetVertexCount(2);
				lineRenderer.SetPosition(0, Vector3.zero);
				lineRenderer.SetPosition(1, Vector3.zero);
				lastPos = Vector3.one * float.MaxValue;
				linePoints.Clear();
				lineCount = 0;
				drawLine = false;
				canDrawLine = false;
				m_currentAngle = 0f;
			}

			return;
		}

		// Draw line
		if (canDrawLine) {

			if (Input.GetMouseButtonDown(0)) {
				firstPos = Input.mousePosition;
				firstPos.z = 0.5f;
				firstPos = Camera.main.ScreenToWorldPoint(firstPos);
				linePoints.Add(firstPos);
				drawLine = true;
			}

			if(Input.GetMouseButton(0)) {
				Vector3 mousePos = Input.mousePosition;
				mousePos.z = 0.5f;
				Vector3 mouseWorld = Camera.main.ScreenToWorldPoint(mousePos);

				// Check if threshold is meet
				float dist = Vector3.Distance(lastPos, mouseWorld);
				if (dist <= threshold) {
					return;
				}

				// Add position to list and update last position
				linePoints.Add(mouseWorld);
				lastPos = mouseWorld;

				// Update the lines
				UpdateLine();
			}

			// Reset everything
			if(Input.GetMouseButtonUp(0)) {
				lineRenderer.SetVertexCount(2);
				lineRenderer.SetPosition(0, Vector3.zero);
				lineRenderer.SetPosition(1, Vector3.zero);
				lastPos = Vector3.one * float.MaxValue;
				linePoints.Clear();
				lineCount = 0;
				drawLine = false;
				canDrawLine = false;
				m_currentAngle = 0f;
			}
		}

		TurnAction();
	}

	void UpdateLine()
	{
		lineRenderer.SetVertexCount(linePoints.Count);

		for (int i = lineCount; i < linePoints.Count; i++) {
			lineRenderer.SetPosition(i, linePoints[i]);
		}

		lineCount = linePoints.Count;
	}

	void TurnAction()
	{
		if (Input.GetMouseButtonDown(0)) {
			m_firstPos = Input.mousePosition;
			m_currentPos = Vector3.zero;
			m_mouseDownTime = Time.deltaTime;
			m_currentAngle = 0f;
		}

		if (Input.GetMouseButton(0)) {
			if (m_checkRate < m_mouseDownTime) {
				Vector3 prevPos = m_currentPos;
				m_currentPos = Input.mousePosition - m_firstPos;

				if (prevPos.magnitude > 0f && m_currentPos.magnitude > 0f) {
					float dot = Mathf.Min(1f, Vector3.Dot(m_currentPos, prevPos) / (prevPos.magnitude * m_currentPos.magnitude));
					float deltaAng = Mathf.Acos(dot);
					float direction = Vector3.Cross(m_currentPos, prevPos).z > 0f ? 1f : -1f;
					m_currentAngle += deltaAng * direction;

					// Rotate Directions
					if (m_currentAngle > Mathf.PI) {
						Turn(TurnDirection.RIGHT180);
					}
					else if (m_currentAngle > Mathf.PI/2f) {
						Turn(TurnDirection.RIGHT90);
					}
					else if (m_currentAngle < -Mathf.PI) {
						Turn(TurnDirection.LEFT180);
					}
					else if (m_currentAngle < -Mathf.PI/2f) {
						Turn(TurnDirection.LEFT90);
					}
				}

				m_mouseDownTime = 0f;
			}

			m_mouseDownTime += Time.deltaTime;
		}
	}

	protected virtual void Turn(TurnDirection t)
	{
		if (onTurn != null) {
			onTurn(t);
		}
	}

	public static TurnDirection ParseDirection(string pDirection)
	{
		switch (pDirection.ToLower())
		{
			case "left90":
				return TurnDirection.LEFT90;
			case "right90":
				return TurnDirection.RIGHT90;
			case "right180":
				return TurnDirection.RIGHT180;
			case "left180":
				return TurnDirection.LEFT180;
			default:
				return TurnDirection.RIGHT90;
		}
	}

	public static bool EvaluatePolarity (TurnDirection t)
	{
		switch (t)
		{
			case TurnDirection.LEFT180:
				return false;
			case TurnDirection.LEFT90:
				return false;
			case TurnDirection.RIGHT180:
				return true;
			case TurnDirection.RIGHT90:
				return true;
			default:
				return true;
		}
	}
}
