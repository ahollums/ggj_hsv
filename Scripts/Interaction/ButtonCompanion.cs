﻿using UnityEngine;
using System.Collections;
//using UnityEditor;


public class ButtonCompanion : MonoBehaviour {

	[HideInInspector]
	public UITweenInspector[] onHover = new UITweenInspector[0];
	protected UITween[] hoverTweens = new UITween[0];
	[HideInInspector]
	public UITweenInspector[] onClick = new UITweenInspector[0];
	protected UITween[] clickTweens = new UITween[0];

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
