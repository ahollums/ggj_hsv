﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider))]
public class EventTrigger : MonoBehaviour 
{
	protected Collider col;

	/// <summary>
	/// Gets called when the trigger is entered
	/// </summary>
	/// <param name="trigger">The trigger's GameObject.</param>
	/// <param name="obj">The object that entered the trigger
	public delegate void TriggerDelegate(GameObject trigger, GameObject obj, bool entered);
	public TriggerDelegate onTrigger;

	// Use this for initialization
	protected virtual void Start () 
	{
		col = gameObject.GetComponent<Collider>();
		col.isTrigger = true;
	}

	void OnTriggerEnter(Collider other)
	{
		if(onTrigger != null)
			onTrigger(gameObject, other.gameObject, true);
	}

	void OnTriggerExit(Collider other)
	{
		if(onTrigger != null)
			onTrigger(gameObject, other.gameObject, false);
	}
}
