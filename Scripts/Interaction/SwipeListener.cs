﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public enum SwipeDirection
{
	LEFT = 0,
	RIGHT,
	UP,
	DOWN
}

public class SwipeListener : MonoBehaviour
{
	public static SwipeListener instance;
	public delegate void SwipeDelegate (SwipeDirection d);
	public static SwipeDelegate onSwipe;

	public bool useArrows = false;
	[SerializeField]
	protected float tolerance = 25;
	[SerializeField]
	protected float m_checkRate;
	protected float m_mouseDownTime;
	[SerializeField] bool logging = false;

	protected Vector3 m_firstPos;
	protected Vector3 m_currentSwipe;

	protected LineRenderer lineRenderer;
	public static bool canDrawLine = false;
	protected bool drawLine = false;
	protected Vector3 firstPos = Vector3.one * float.MaxValue;
	protected Vector3 lastPos = Vector3.one * float.MaxValue;

	protected virtual void Awake()
	{
		instance = this;
		m_checkRate = Mathf.Clamp01(m_checkRate);
		lineRenderer = gameObject.AddMissingComponent<LineRenderer>();
	}

	protected void Update()
	{
		if (onSwipe == null) {
			if (drawLine) {
				lineRenderer.SetPosition(0, Vector3.zero);
				lineRenderer.SetPosition(1, Vector3.zero);
				drawLine = false;
				canDrawLine = false;
			}

			return;
		}

		if (useArrows) {
			ListenArrows ();
		}
		else {
			// Draw line
			if (canDrawLine) {
				if (Input.GetMouseButtonDown(0)) {
					firstPos = Input.mousePosition;
					firstPos.z = 0.5f;
					firstPos = Camera.main.ScreenToWorldPoint(firstPos);
					drawLine = true;
				}

				if(Input.GetMouseButton(0)) {
					lastPos = Input.mousePosition;
					lastPos.z = 0.5f;
					lastPos = Camera.main.ScreenToWorldPoint(lastPos);
					lineRenderer.SetPosition(0, firstPos);
					lineRenderer.SetPosition(1, lastPos);
				}

				if(Input.GetMouseButtonUp(0)) {
					lineRenderer.SetPosition(0, Vector3.zero);
					lineRenderer.SetPosition(1, Vector3.zero);
					drawLine = false;
				}
			}

			SwipeAction();
		}
	}

	void ListenArrows()
	{
		if (Input.GetKeyDown (KeyCode.LeftArrow)) {
			Swipe (SwipeDirection.LEFT);
		}

		if (Input.GetKeyDown (KeyCode.RightArrow)) {
			Swipe (SwipeDirection.RIGHT);
		}

		if (Input.GetKeyDown (KeyCode.UpArrow)) {
			Swipe (SwipeDirection.UP);
		}

		if (Input.GetKeyDown (KeyCode.DownArrow)) {
			Swipe (SwipeDirection.DOWN);
		}
	}

	public void SwipeAction()
	{
		if (Input.GetMouseButtonDown(0)) {
			m_firstPos = Input.mousePosition;
			m_mouseDownTime = Time.deltaTime;
		}

		if (Input.GetMouseButton(0)) {
			if (m_checkRate < m_mouseDownTime) {
				m_currentSwipe = Input.mousePosition - m_firstPos;

				if (m_currentSwipe.magnitude >= tolerance) {
					m_currentSwipe.Normalize();

					// Swipe Directions
					if (m_currentSwipe.y > 0f && m_currentSwipe.x > -0.5f && m_currentSwipe.x < 0.5f) { // Swipe upwards
						Swipe(SwipeDirection.UP);
					}
					else if (m_currentSwipe.y < 0 && m_currentSwipe.x > -0.5f && m_currentSwipe.x < 0.5f) { // Swipe down
						Swipe(SwipeDirection.DOWN);
					}
					else if (m_currentSwipe.x < 0 && m_currentSwipe.y > -0.5f && m_currentSwipe.y < 0.5f) { // Swipe left
						Swipe(SwipeDirection.LEFT);
					}
					else if (m_currentSwipe.x > 0 && m_currentSwipe.y > -0.5f && m_currentSwipe.y < 0.5f) { // Swipe right
						Swipe(SwipeDirection.RIGHT);
					}
				}

				m_mouseDownTime = 0f;
			}

			m_mouseDownTime += Time.deltaTime;
		}
	}

	protected virtual void Swipe(SwipeDirection d)
	{
		Log (string.Format("Swiped [{0}]", d.ToString()));
		if(onSwipe != null) {
			onSwipe(d);
		}
	}

	public static SwipeDirection ParseDirection(string pDirection)
	{
		switch (pDirection.ToLower())
		{
			case "left":
				return SwipeDirection.LEFT;
			case "right":
				return SwipeDirection.RIGHT;
			case "up":
				return SwipeDirection.UP;
			case "down":
				return SwipeDirection.DOWN;
			default:
				return SwipeDirection.LEFT;
		}
	}

    public static bool EvaluatePolarity (SwipeDirection d)
    {
        switch (d)
        {
            case SwipeDirection.DOWN:
                return false;
            case SwipeDirection.UP:
                return true;
            case SwipeDirection.LEFT:
                return false;
            case SwipeDirection.RIGHT:
                return true;
            default:
                return true;
        }
    }

	protected void Log (string message)
	{
#if UNITY_EDITOR
		if(logging)
			Debug.Log (string.Format ("[{0}]: {1}", this.GetType ().Name, message));
#endif
	}
}
