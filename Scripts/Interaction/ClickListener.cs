﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public enum ClickMethod
{
	LEFT = 0,
	RIGHT,
	DLEFT,
	DRIGHT
}

public class ClickListener : MonoBehaviour
{
	public static ClickListener instance;

	public delegate void ClickDelegate(ClickMethod c);
	public delegate void RemoveComponentDelegate();
	public static ClickDelegate onClick;
	[SerializeField]
	protected float m_doubleClickTime;
	protected float m_lastClickTime;
	protected bool m_overClickable = false;
	protected bool m_oneClickLeft = false;
	protected bool m_oneClickRight = false;
	public static bool listenForDoubleClick;

	protected virtual void Awake()
	{
		instance = this;
	}

	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	protected void Update()
	{
		if (!m_overClickable)
		{
			return;
		}

		// Left Click
		if (Input.GetMouseButtonDown(0)) {
			if (!m_oneClickLeft) { // First click no previous clicks
				m_oneClickLeft = true;

				m_lastClickTime = Time.time;
				if (!listenForDoubleClick) { // Single Click
					Click(ClickMethod.LEFT);
				}
			}
			else { // Found a double click
				m_oneClickLeft = false; // Reset

				if (listenForDoubleClick) { // Double Click
					Click(ClickMethod.DLEFT);
				}
				else { // Single Click
					Click(ClickMethod.LEFT);
				}
			}
		}

		if (m_oneClickLeft) {
			if ((Time.time - m_lastClickTime) > m_doubleClickTime) { // Check time
				m_oneClickLeft = false; // Reset
				Click(ClickMethod.LEFT); // Single Click
			}
		}

		// Right Click
		if (Input.GetMouseButtonDown(1)) {
			if (!m_oneClickRight) { // First click no previous clicks
				m_oneClickRight = true;

				m_lastClickTime = Time.time;
				if (!listenForDoubleClick) { // Single Click
					Click(ClickMethod.RIGHT);
				}
			}
			else { // Found a double click
				m_oneClickRight = false; // Reset

				if (listenForDoubleClick) { // Double Click
					Click(ClickMethod.DRIGHT);
				}
				else { // Single Click
					Click(ClickMethod.RIGHT);
				}
			}
		}

		if (m_oneClickRight) {
			if ((Time.time - m_lastClickTime) > m_doubleClickTime) { // Check time
				m_oneClickRight = false; // Reset
				Click(ClickMethod.RIGHT); // Single Click
			}
		}
	}

	public static void SetObjectListener(GameObject go)
	{
		InteractiveObject iObj = go.AddMissingComponent<InteractiveObject>();
		if (iObj.onMouseOver != null) {
			iObj.onMouseOver -= instance.OverObject;
		}
		iObj.onMouseOver += instance.OverObject;

		if (iObj.onMouseExit != null) {
			iObj.onMouseExit -= instance.ExitObject;
		}
		iObj.onMouseExit += instance.ExitObject;
	}

	protected virtual void Click(ClickMethod c)
	{
		if (onClick != null) {
			onClick(c);
		}

		m_lastClickTime = 0f;
	}

	protected virtual void OverObject()
	{
		m_overClickable = true;
	}

	protected virtual void ExitObject()
	{
		m_overClickable = false;
	}

	public static ClickMethod ParseDirection(string pDirection)
	{
		switch (pDirection.ToLower())
		{
			case "left":
				return ClickMethod.LEFT;
			case "right":
				return ClickMethod.RIGHT;
			case "dleft":
				return ClickMethod.DLEFT;
			case "dright":
				return ClickMethod.DRIGHT;
			default:
				return ClickMethod.LEFT;
		}
	}
}
