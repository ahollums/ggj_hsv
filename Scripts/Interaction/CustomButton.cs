﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(ButtonCompanion))]
public class CustomButton : Button {
	public delegate void OnMouseEnter();
	public OnMouseEnter onMouseEnter;
	public delegate void OnMouseLeave();
	public OnMouseLeave onMouseLeave;
	public delegate void OnMouseDown();
	public OnMouseDown onMouseDown;
	public delegate void OnMouseUp();
	public OnMouseUp onMouseUp;
	// Use this for initialization

	bool logging = true;

	protected override void Start ()
	{
		base.Start ();
	}

	public override void OnPointerEnter (UnityEngine.EventSystems.PointerEventData eventData)
	{
		base.OnPointerEnter (eventData);
		if(onMouseEnter!=null) onMouseEnter();
	}

	public override void OnPointerExit (UnityEngine.EventSystems.PointerEventData eventData)
	{
		base.OnPointerExit (eventData);
		if(onMouseLeave!=null) onMouseLeave();
	}

	public override void OnPointerDown (UnityEngine.EventSystems.PointerEventData eventData)
	{
		base.OnPointerDown (eventData);
		if(onMouseDown != null) onMouseDown();
	}

	public override void OnPointerUp (UnityEngine.EventSystems.PointerEventData eventData)
	{
		base.OnPointerUp (eventData);
		if(onMouseUp != null) onMouseUp();

		this.DoStateTransition(SelectionState.Normal,true);
	}

	protected virtual void Log(string message)
	{
		if(logging)
			Debug.Log ("[CustomButton]: "+message);
	}
}
