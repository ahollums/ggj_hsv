﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Objective : MonoBehaviour 
{
	[SerializeField] protected List<Challenge> challenges = new List<Challenge>();
    protected int currentIndex = 0;
    protected float demoTime = 1.5f;
	[SerializeField] bool logging = true;

    public virtual void Initialize(params Challenge[] pChallenges) 
    {
        challenges = new List<Challenge>(pChallenges);
    }
        
    public virtual void BeginObjective()
    {
        StartCoroutine(SimonSays(HandoffControl));
    }

    protected virtual void HandoffControl()
    {
        if (Current != null)
        {
            RegisterChallenge(Current, true);
            Current.Begin();
        }
        else
        {
            Log("There were no challenges!", LogType.Warning);
        }
    }

    protected IEnumerator SimonSays(System.Action action)
    {
		UIManager.ShowDemoMode(true);
        UIManager.CountText = "3";
        yield return new WaitForSeconds(.5f);
        UIManager.CountText = "2";
        yield return new WaitForSeconds(.5f);
        UIManager.CountText = "1";
        if (Current != null) Current.FocusHotspot();
        yield return new WaitForSeconds(1.5f);
        UIManager.CountText = null;

        foreach (Challenge c in challenges)
        {
            c.Demonstrate();
            yield return new WaitForSeconds(demoTime);
        }

		UIManager.ShowDemoMode(false);
        yield return null;
        action();
    }

    protected void RegisterChallenge (Challenge c, bool register)
    {
        if (register)
        {
            RegisterChallenge(c, false); // ensure we don't double stack 
            c.onComplete += ChallengeComplete;
            c.onFail += ChallengeFailed;
        }
        else
        {
            c.onComplete -= ChallengeComplete;
            c.onFail -= ChallengeFailed;
        }
    }

	public virtual void UpdateObjective()
	{
		
	}

	public virtual void CompleteObjective()
	{
        Log("Objective Complete!");
		UIManager.ShowCenterMessage("Objective Complete!", OCDColors.GOOD, 1f);
		ObjectiveSystem.ObjectiveComplete (this);
	}

	public virtual void FailObjective()
	{
        Log("Objective Failed!");
//        UIManager.ShowCenterMessage("Failure...", Color.magenta, 5f);
		ObjectiveSystem.ObjectiveFailed (this);
	}

	public virtual void ChallengeComplete(Challenge c)
	{
		Log(string.Format("Challenge [{0}] completed!", c.ID));
        UIManager.ShowCenterMessage("Good job!", OCDColors.GOOD, .5f);
        RegisterChallenge(c, false);
        if (++currentIndex == challenges.Count)
        {
            CompleteObjective();
        }
        else
        {
            if (Current != null)
            {
                RegisterChallenge(Current, true);
                Current.Begin();
            }
            else
                Log("ERROR!");
        }
	}

    public virtual void ChallengeFailed (Challenge c)
    {
        Log(string.Format("Challenge [{0}] failed!", c.ID));
        UIManager.ShowCenterMessage("No! Wrong!", OCDColors.BAD, 1f);
        SoundManger.Frustration();
        UIManager.ShowDamageTaken(true);
        RegisterChallenge(c, false);
        Player.PlayerHealth.Hit(1);
        if (++ObjectiveSystem.Failures == ObjectiveSystem.FailTolerance)
        {
            FailObjective();
        }
        else
            ResetObjective();
    }

    public virtual void ResetObjective()
    {
        Log("Resetting objective...");
        currentIndex = 0;
        UIManager.ShowCenterMessage("Have to get this right...", OCDColors.NEUTRAL, 1f);
        ActionHandle.DelayedAction(2f, BeginObjective);
    }

    public Challenge Current
    {
        get
        {
            return challenges.InRange(currentIndex) ? challenges[currentIndex] : null;
        }
    }

    void Log (string message, LogType type = LogType.Log)
    {
        message = string.Format("[{0}]: {1}", this.GetType().Name, message);
        switch (type)
        {
            case LogType.Log:
                if(logging) Debug.Log(message);
                break;
            case LogType.Warning:
                Debug.LogWarning(message);
                break;
            case LogType.Error:
                Debug.LogError(message);
                break;
        }
    }
}
