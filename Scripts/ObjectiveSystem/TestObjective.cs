﻿using UnityEngine;
using System.Collections;

public class TestObjective : Objective 
{
	public bool useClicks;
	public bool useSwipes;
	public bool useTurns;

	protected void Awake()
    {
        Initialize();
    }

    public override void Initialize(params Challenge[] pChallenges)
    {
        base.Initialize();
//		if (useClicks)
//		{
//			ClickListener.SetObjectListener(GameObject.Find("Capsule"));
//			challenges.Add(new ClickChallenge("C001", ClickMethod.LEFT));
//			challenges.Add(new ClickChallenge("C002", ClickMethod.RIGHT));
//			challenges.Add(new ClickChallenge("C003", ClickMethod.DLEFT));
//			challenges.Add(new ClickChallenge("C004", ClickMethod.LEFT));
//			challenges.Add(new ClickChallenge("C005", ClickMethod.DRIGHT));
//		}
//
//		if (useSwipes)
//		{
//			challenges.Add(new SwipeChallenge("S001", SwipeDirection.DOWN));
//			challenges.Add(new SwipeChallenge("S002", SwipeDirection.DOWN));
//			challenges.Add(new SwipeChallenge("S003", SwipeDirection.LEFT));
//			challenges.Add(new SwipeChallenge("S004", SwipeDirection.RIGHT));
//			challenges.Add(new SwipeChallenge("S005", SwipeDirection.DOWN));
//		}
//
//		if (useTurns)
//		{
//			challenges.Add(new TurnChallenge("T001", TurnDirection.LEFT90));
//			challenges.Add(new TurnChallenge("T002", TurnDirection.RIGHT90));
//			challenges.Add(new TurnChallenge("T003", TurnDirection.LEFT360));
//			challenges.Add(new TurnChallenge("T004", TurnDirection.RIGHT180));
//		}

		BeginObjective();
    }

	public override void CompleteObjective()
	{
		base.CompleteObjective();
	}

	public override void FailObjective()
	{
		base.FailObjective();
	}
}
