﻿using UnityEngine;
using System.Collections;

public class ClickChallenge : Challenge
{
	public ClickMethod m_clickType;

	public ClickChallenge(string id, string pFocusObjectName, string pHotspotName, ClickMethod c) : base(id, pFocusObjectName, pHotspotName)
	{
		m_clickType = c;
	}

	protected override void RegisterFocus (bool register)
	{
		base.RegisterFocus (register);
		if (!register) {
			ClickListener.onClick -= OnClick;
		}
	}

	public override void Begin()
	{
		base.Begin();
		ClickListener.onClick = OnClick;
		ClickListener.listenForDoubleClick = false;
		if (m_clickType == ClickMethod.DLEFT || m_clickType == ClickMethod.DRIGHT) {
			ClickListener.listenForDoubleClick = true;
		}

		if (focusObject != null) {
			ClickListener.SetObjectListener(focusObject.gameObject);
		}
	}

	public override void Complete()
	{
		base.Complete();
		ClickListener.onClick -= OnClick;
	}

	public void OnClick(ClickMethod c)
	{
		if (!focused)
			return;
		
        if (Tweener)
            Tweener.PlayTween(c);

        if (ChallengeListener)
            ChallengeListener.GestureHeard(true); // has no polarity, just pass true

		if (c == m_clickType) {
			Complete();
		}
		else {
			Fail();
		}
	}

    public override void Demonstrate()
    {
        base.Demonstrate();
        Log(string.Format("Show [{0}] Click Indicator!", m_clickType.ToString()));
        UIManager.ShowClickIndicator(m_clickType, true);
    }

	public override void Fail()
	{
		base.Fail();
	}

	public override void Reset()
	{
		base.Reset();
	}
}
