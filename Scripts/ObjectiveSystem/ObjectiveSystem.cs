﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectiveSystem : MonoBehaviour 
{
	public static ObjectiveSystem instance;
	public List<Objective> objectives = new List<Objective>();
    protected int currentIndex = 0;
    [Tooltip("The amount of time to give the \"Begin Day\" animation.")]
    public float beginDayTime = 3f;
    protected int currentDay = 0;
    protected int failures = 0;
    [SerializeField] protected int failuresAllowed = 3;

    [SerializeField] string xmlSource = "objectives";
	public CustomTweenRotation doorRotation;
	[SerializeField] bool logging = true;

	protected virtual void Awake()
	{
		instance = this;
    }

    protected virtual void Start()
    {
        objectives = ObjectiveParser.ParseObjectives(xmlSource, transform);
        currentDay = ObjectiveParser.ParseDayNumber(xmlSource);
       
        if(Player.PlayerHealth)
            Player.PlayerHealth.HitPoints = failuresAllowed;

        StartCoroutine(DelayBegin());
    }

    IEnumerator DelayBegin()
    {
        if (currentDay == 0)
        {
            UIManager.ShowTutorial(true);
            while (UIManager.InMainMenu)
                yield return null;

        }

        UIManager.BeginDay(currentDay, beginDayTime);
        yield return new WaitForSeconds(beginDayTime);
        LightManager.OnLights();
        while (UIManager.InMainMenu)
            yield return null;
        Begin();
    }

    public void Begin()
    {
        if (Current)
        {
            Current.BeginObjective();
        }
        else
            Log("No objectives!", LogType.Warning);
    }

	protected virtual void Update()
	{
		foreach (Objective o in objectives)
            o.UpdateObjective ();

        if (Input.GetKeyDown(KeyCode.Space))
        {
            SystemFailed();
        }

        if (Input.GetKeyDown(KeyCode.A))
        {
            SystemComplete();
        }
	}

	public static void ObjectiveComplete(Objective o)
	{
		++Player.Score;
        if (++instance.currentIndex == instance.objectives.Count)
        {
			CamController.Reset();
            if (instance.doorRotation != null)
            {
                ActionHandle.DelayedAction(1f, () =>
                    {
                        SoundManger.PlaySoundEffect(SoundType.DOOR);
                        instance.doorRotation.PlayForward(); 
                    });
            }
            ActionHandle.DelayedAction(4f, instance.SystemComplete);
        }
        else
        {
            UIManager.ShowCenterMessage("Yes yes, good...", OCDColors.GOOD, 2f);
            SoundManger.Play(AudioEnum.CorrectClip);
            ActionHandle.DelayedAction(2f, Current.BeginObjective);
        }
	}

	public static void ObjectiveFailed(Objective o)
	{
        Log("Objective failed...");
        ActionHandle.DelayedAction(2f, instance.SystemFailed);
	}

    public void SystemComplete()
    {
        UIManager.ShowVictoryPanel(true);
        SoundManger.Play(AudioEnum.YouWinClip);
    }

    public void SystemFailed()
    {
        UIManager.ShowFailurePanel(true);
        SoundManger.Play(AudioEnum.YouLoseClip);
    }

    public void LoadNextDay()
    {
        currentDay++;
		++Player.DaysEndured;
        string nXmlSource = SequenceGenerator.GenerateXml(currentDay);
        objectives = ObjectiveParser.ParseObjectivesXmlString(nXmlSource, transform);
        currentIndex = 0;
        StartCoroutine(DelayBegin());
		if(instance.doorRotation != null) { instance.doorRotation.PlayReverse(); }
	}

    public void LoadDay(int dayNum, bool resetAll = true)
    {
        if (resetAll)
        {
            failures = 0;
            currentIndex = 0;
            if (Player.PlayerHealth)
                Player.PlayerHealth.HitPoints = failuresAllowed;
        }

        foreach (Objective o in objectives)
        {
            DestroyImmediate(o);
        }

        currentDay = dayNum;
        if (dayNum > 0)
        {
            string nXmlSource = SequenceGenerator.GenerateXml(currentDay);
            objectives = ObjectiveParser.ParseObjectivesXmlString(nXmlSource, transform);
        }
        else
        {
            objectives = ObjectiveParser.ParseObjectives(xmlSource, transform);
        }

        currentIndex = 0;
        StartCoroutine(DelayBegin());
    }

    public void ResetSystem()
    {
        Log("Resetting system...");
        UIManager.ShowFailurePanel(false);
		Player.playedHighScoreClip = false;
        if (currentDay == 0)
		{
			Player.Score = 0;
            LoadDay(0, true);
		}
        else
		{
			Player.Score = 2;
            LoadDay(1, true);
		}
    }

    public static Objective Current
    {
        get
        {
            return instance && instance.objectives.InRange(instance.currentIndex) ? instance.objectives[instance.currentIndex] : null;
        }
    }

    public static int Failures
    {
        get
        {
            return instance ? instance.failures : 0;
        }
        set
        {
            if(instance) instance.failures = value;
        }
    }

    public static float FailTolerance
    {
        get
        {
            return instance ? instance.failuresAllowed : 3;
        }
    }

    public static int CurrentDay
    {
        get
        {
            return instance ? instance.currentDay : 0;
        }
    }

    static void Log (string message, LogType type = LogType.Log)
    {
        message = string.Format("[{0}]: {1}", "ObjectiveSystem", message);
        switch (type)
        {
            case LogType.Log:
#if UNITY_EDITOR
                if(instance.logging) Debug.Log(message);
#endif
                break;
            case LogType.Warning:
                Debug.LogWarning(message);
                break;
            case LogType.Error:
                Debug.LogError(message);
                break;
        }
    }
}
