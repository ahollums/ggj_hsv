﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(ObjectiveSystem))]
public class ObjectiveSystemEditor : Editor
{
    ObjectiveSystem system;
    bool showTest = false;
    int loadDay = 0;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        system = target as ObjectiveSystem;
        if (!Application.isPlaying)
        {
            EditorGUILayout.HelpBox("Testing features are available on this component at runtime.", MessageType.Info);
            return;
        }

        if ((showTest = EditorGUILayout.Foldout(showTest, "Testing")))
        {
            loadDay = EditorGUILayout.IntField(new GUIContent("Load Day", "The day you wish to load."), loadDay);
            if (GUILayout.Button(string.Format("LoadDay ({0})", loadDay.ToString())))
            {
                system.LoadDay(loadDay, true);
            }
        }
    }
}
