﻿using UnityEngine;
using System.Collections;

public class SwipeChallenge : Challenge
{
	public SwipeDirection direction;

	public SwipeChallenge (string pID, string pFocusObjectName, string pHotspotName, SwipeDirection pDirection) : base(pID, pFocusObjectName, pHotspotName)
	{
		direction = pDirection;
	}

	protected override void RegisterFocus (bool register)
	{
		base.RegisterFocus (register);
		if (!register) {
			SwipeListener.onSwipe -= OnSwipe;
			SwipeListener.canDrawLine = false;
		}
	}

	public override void Begin()
	{
		base.Begin();
	}

	protected override void FocusHeld()
	{
		base.FocusHeld();
		SwipeListener.onSwipe = OnSwipe;
		SwipeListener.canDrawLine = true;
	}

	protected override void FocusReleased()
	{
		base.FocusReleased();
		SwipeListener.onSwipe -= OnSwipe;
		SwipeListener.canDrawLine = false;
	}

	public override void Complete ()
	{
		base.Complete ();
		SwipeListener.onSwipe -= OnSwipe;
	}

	public void OnSwipe(SwipeDirection d)
    {
		if (!focused)
			return;
		
        if (Tweener)
            Tweener.PlayTween(d);

        if (ChallengeListener)
            ChallengeListener.GestureHeard(SwipeListener.EvaluatePolarity(d));

		if (d == direction) {
			Complete();
		}
		else {
			Fail();
		}
	}

    public override void Demonstrate()
    {
        base.Demonstrate();
        Log(string.Format("Show [{0}] Swipe Indicator!", direction.ToString()));
        UIManager.ShowSwipeIndicator(direction, true);
    }

	public override void Fail ()
    {
		base.Fail ();
	}

	public override void Reset ()
	{
		base.Reset ();
	}
}
