﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

public class ObjectiveParser 
{
    public static List<Objective> ParseObjectives(string pXmlPath, Transform parent)
    {
        List<Objective> objectives = new List<Objective>();
        XmlNodeList nodeList = XmlTools.GetXmlNodes(pXmlPath);
        foreach (XmlNode node in nodeList)
        {
            Objective thisObjective = ParseObjectiveNode(node, parent);
            if (thisObjective != null)
            {
                objectives.Add(thisObjective);
            }
            else
            {
                Log(string.Format("There was an error parsing the node [{0}] in the xml file [{1}]", node.Name, pXmlPath), LogType.Error);
            }
        }
        return objectives;
    }

    public static List<Objective> ParseObjectivesXmlString(string xmlText, Transform parent)
    {
        List<Objective> objectives = new List<Objective>();
        XmlDocument xmlDoc = new XmlDocument();
        try
        {
            xmlDoc.LoadXml(xmlText);
        }catch (System.Exception error)
        {
            Log(string.Format("Loading file [{0}] threw an exception: {1}", xmlText, error.ToString()), LogType.Error);
            return objectives;
        }
 
        XmlNode root = xmlDoc.DocumentElement;
        XmlNodeList nodeList = root.ChildNodes;
        foreach (XmlNode node in nodeList)
        {
            Objective thisObjective = ParseObjectiveNode(node, parent);
            if (thisObjective != null)
            {
                objectives.Add(thisObjective);
            }
            else
            {
                Log(string.Format("There was an error parsing the node [{0}] in the xml file [{1}]", node.Name, xmlText), LogType.Error);
            }
        }
        return objectives;
    }

    static Objective ParseObjectiveNode(XmlNode node, Transform parent)
    {
        string type = XmlTools.AttributeValue(node, "Type");
        string id = XmlTools.AttributeValue(node, "ID");
        GameObject objectiveObject = new GameObject();
        objectiveObject.name = string.Format("Obj_{0}_{1}", type, id);
        objectiveObject.transform.parent = parent;
        Objective thisObjective;
        switch (type)
        {
            default:
                thisObjective = objectiveObject.AddComponent<Objective>();
                break;
        }
         
        List<Challenge> challenges = new List<Challenge>();
        foreach (XmlNode childNode in node.ChildNodes)
        {
            if (childNode.Name.ToLower() == "challenge")
            {
                Challenge thisChallenge = ParseChallengeNode(childNode);
                if (thisChallenge != null)
                {
                    challenges.Add(thisChallenge);
                }
                else
                {
                    Log("Error parsing a challenge from the Xml file.", LogType.Error);
                    Log("Node: " + childNode.OuterXml + childNode.InnerText, LogType.Error);
                }
            }
        }
        thisObjective.Initialize(challenges.ToArray());
        return thisObjective;
    }

    public static int ParseDayNumber(string pXmlPath)
    {
        XmlNode rootNode = XmlTools.GetRootNode(pXmlPath);
        int dayNum = -1;
        if (rootNode == null)
        {
            Log(string.Format("Could not parse the day number from [{0}]!", pXmlPath), LogType.Warning);
            return dayNum;
        }

        string dayNumString = XmlTools.AttributeValue(rootNode, "Day");
        if (int.TryParse(dayNumString, out dayNum))
            return dayNum;
        else
        {
            Log("The day number attribute must be an integer.", LogType.Warning);
            return dayNum;
        }
    }

    static Challenge ParseChallengeNode(XmlNode node)
    {
        string type = XmlTools.AttributeValue(node, "Type");
        string id = XmlTools.AttributeValue(node, "ID");
        string focusObject = XmlTools.AttributeValue(node, "Focus");
        string hotspotName = XmlTools.AttributeValue(node, "Hotspot");
		string direction;
		Challenge thisChallenge = null;
        switch (type)
        {
            case "Swipe":
                direction = XmlTools.AttributeValue(node, "Direction");
                if (string.IsNullOrEmpty(direction))
                {
                    Log(string.Format("Couldn't direction of swipe challenge [{0}]", string.IsNullOrEmpty(id) ? "NO ID" : id), LogType.Error);
                    direction = "left";
                }
                thisChallenge = new SwipeChallenge(id, focusObject, hotspotName, SwipeListener.ParseDirection(direction));
                break;
			case "Turn":
				direction = XmlTools.AttributeValue(node, "Direction");
				if (string.IsNullOrEmpty(direction))
				{
					Log(string.Format("Couldn't direction of turn challenge [{0}]", string.IsNullOrEmpty(id) ? "NO ID" : id), LogType.Error);
					direction = "right90";
				}
				thisChallenge = new TurnChallenge(id, focusObject, hotspotName, TurnListener.ParseDirection(direction));
				break;
			case "Click":
				direction = XmlTools.AttributeValue(node, "Direction");
				if (string.IsNullOrEmpty(direction))
				{
					Log(string.Format("Couldn't direction of click challenge [{0}]", string.IsNullOrEmpty(id) ? "NO ID" : id), LogType.Error);
					direction = "left";
				}
				thisChallenge = new ClickChallenge(id, focusObject, hotspotName, ClickListener.ParseDirection(direction));
				break;
		}

        return thisChallenge;
    }

    static void Log (string message, LogType type = LogType.Log)
    {
        message = string.Format("[{0}]: {1}", "ObjectiveParser", message);
        switch (type)
        {
            case LogType.Log:
#if UNITY_EDITOR
                Debug.Log(message);
#endif
                break;
            case LogType.Warning:
                Debug.LogWarning(message);
                break;
            case LogType.Error:
                Debug.LogError(message);
                break;
        }
    }
}
