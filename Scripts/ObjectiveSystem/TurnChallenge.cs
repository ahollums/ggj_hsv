﻿using UnityEngine;
using System.Collections;

public class TurnChallenge : Challenge
{
	public TurnDirection m_turnType;

	public TurnChallenge(string id, string pFocusObjectName, string pHotspotName, TurnDirection t) : base(id, pFocusObjectName, pHotspotName)
	{
		m_turnType = t;
	}

	protected override void RegisterFocus (bool register)
	{
		base.RegisterFocus (register);
		if (!register) {
			TurnListener.onTurn -= OnTurn;
			TurnListener.canDrawLine = false;
		}
	}

	protected override void FocusHeld()
	{
		base.FocusHeld();
		TurnListener.onTurn = OnTurn;
		TurnListener.canDrawLine = true;
	}

	protected override void FocusReleased()
	{
		base.FocusReleased();
		TurnListener.onTurn -= OnTurn;
		TurnListener.canDrawLine = false;
	}

	public override void Complete()
	{
		base.Complete();
		TurnListener.onTurn -= OnTurn;
	}

	public void OnTurn(TurnDirection t)
	{
		if (!focused)
			return;
		
        if (Tweener)
            Tweener.PlayTween(t);

        if (ChallengeListener)
            ChallengeListener.GestureHeard(TurnListener.EvaluatePolarity(t));
        
		if (t == m_turnType) {
			Complete();
		}
		else {
			Fail();
		}
	}

    public override void Demonstrate()
    {
        base.Demonstrate();
        Log(string.Format("Show [{0}] Turn Indicator!", m_turnType.ToString()));
        UIManager.ShowTurnIndicators(m_turnType, true);
    }

	public override void Fail()
	{
		base.Fail();
	}

	public override void Reset()
	{
		base.Reset();
	}
}
