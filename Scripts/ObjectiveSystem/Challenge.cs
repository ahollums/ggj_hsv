﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Challenge 
{
	public delegate void ChallengeDelegate(Challenge c);
	public ChallengeDelegate onComplete;
	public ChallengeDelegate onFail;
	public ChallengeDelegate onBegin;
    public Transform focusObject;
    public string hotspotName;
    protected HighlightControl highlighter;
	public string ID = "NO ID";

	protected bool focused = false;

    public Challenge (string pID, string pFocusObjectName, string pHotspotName)
    {
        ID = pID;
        focusObject = ObjectFinder.Find(pFocusObjectName);
        highlighter = ObjectFinder.FindObjectType<HighlightControl>();
        hotspotName = pHotspotName;
    }

    protected virtual void RegisterFocus(bool register)
    {
        if (focusObject == null)
        {
            Log(string.Format("The challenge [{0}] had no focus object!", ID), LogType.Warning);
            return;
        }
			
//        focusObject.gameObject.AddMissingComponent<MeshCollider>();
        InteractiveObject iObject = focusObject ? focusObject.gameObject.AddMissingComponent<InteractiveObject>() : null;
        if (iObject == null)
        {
            Log(string.Format("Problem adding the InteractiveObject script to [{0}] in Challenge [{1}]", focusObject.name, ID), LogType.Warning);
            return;
        }

        if (register)
        {
            RegisterFocus(false);
            if (highlighter && focusObject)
            {
                ActionHandle.DelayedAction(0.75f, () => { highlighter.Highlight(focusObject.gameObject); });
            }
            FocusHotspot();
            iObject.onMouseDown += FocusHeld;
            iObject.onMouseUp += FocusReleased;
        }
        else
        {
            if (highlighter && focusObject)
            {
                highlighter.UnHighlight(focusObject.gameObject);
            }
            iObject.onMouseDown -= FocusHeld;
            iObject.onMouseUp -= FocusReleased;
        }

		focused = register;
    }

    public void FocusHotspot()
    {
        if (!string.IsNullOrEmpty(hotspotName))
            CamController.SetTarget(hotspotName);
        else
            Log(string.Format("There was no hotspot associated with Challenge [{0}]!", ID), LogType.Warning);
    }

	public virtual void Begin()
	{
		if (onBegin != null)
			onBegin (this);
        RegisterFocus(true);
	}

	public virtual void Complete()
	{
		if (onComplete != null)
			onComplete (this);
        RegisterFocus(false);
	}

	public virtual void Fail()
	{
		if (onFail != null)
			onFail (this);
        RegisterFocus(false);
	}

	public virtual void Reset()
	{
        RegisterFocus(true);
	}

    public virtual void Demonstrate()
    {
        FocusHotspot();
        Highlight(true);
    }

    protected void Highlight(bool pActive = true)
    {
        if (highlighter && focusObject)
        {
            if(pActive)
                highlighter.Highlight(focusObject.gameObject);
            else
                highlighter.UnHighlight(focusObject.gameObject);
        }
    }

    protected virtual void FocusHeld()
    {
        Log("FocusHeld");
    }

    protected virtual void FocusReleased()
    {
        Log("FocusReleased");
    }

    protected CustomTween Tweener
    {
        get
        {
            return focusObject ? focusObject.GetComponent<CustomTween>() : null;
        }
    }

    protected ChallengeObject ChallengeListener
    {
        get
        {
            return focusObject ? focusObject.GetComponent<ChallengeObject>() : null;
        }
    }

    protected virtual void Log (string message, LogType type = LogType.Log)
	{
        message = string.Format("[{0}]: {1}", GetType().Name, message);
        switch (type)
        {
            case LogType.Log:
#if UNITY_EDITOR
                Debug.Log(message);
#endif
                break;
            case LogType.Warning:
                Debug.LogWarning(message);
                break;
            case LogType.Error:
                Debug.LogError(message);
                break;
        }
	}
}
