﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UITweenAlpha : UITween {
	[HideInInspector]
	public SpriteRenderer graphic;
	[HideInInspector]
	public MaskableGraphic secondaryGraphic;

	[Tooltip("The starting value of the graphic's alpha.")] 
	public float from = 0f;
	[Tooltip("The ending value of the graphic's alpha.")] 
	public float to = 1f;

	public bool recursiveTween = false;

	protected override void Start()
	{
		if(!graphic && !secondaryGraphic)
		{
			graphic = GetComponent<SpriteRenderer>();
			if(graphic == null) secondaryGraphic = GetComponent<MaskableGraphic>();
		}

		if(recursiveTween)
			SetChildren();

		base.Start ();
	}

	void SetChildren()
	{
		foreach(Transform child in transform)
		{
			UITweenAlpha childTween = child.gameObject.AddComponent<UITweenAlpha>();
			childTween.type = type;
			childTween.to = to;
			childTween.from = from;
			childTween.recursiveTween = recursiveTween;
            childTween.resetOnStart = resetOnStart;
			onForward+=childTween.PlayForward;
			onReverse+=childTween.PlayReverse;
		}
	}

	public override void PlayForward ()
	{
		if(!IsValid) 
		{
			base.PlayForward();
			return;
		}

		if(Alpha == to) 
		{
			if(type == TweenType.LOOP || type == TweenType.PINGPONG) PlayReverse ();
			else return;
		}

		if(isPlaying)
		{
			base.PlayForward ();
			StartCoroutine (TweenRoutine (Alpha,to));
		}
		else
		{
			base.PlayForward ();
			Alpha = from;
			StartCoroutine (TweenRoutine(from, to));
		}
	}
	
	public override void PlayReverse ()
	{
		if(!IsValid) 
		{
			base.PlayReverse();
			return;
		}

		if(Alpha == from) return;
		if(isPlaying)
		{
			base.PlayReverse ();
			StartCoroutine (TweenRoutine (Alpha, from));
		}
		else
		{
			base.PlayReverse ();
			Alpha = to;
			StartCoroutine (TweenRoutine(to, from));
		}
	}
	
	public override void Toggle()
	{
		if(type == TweenType.LOOP)
		{
			if(playingForward)
				PlayReverse ();
			else
				PlayForward ();
			return;
		}
		
		if(isPlaying)
		{
			if(playingForward)
			{
				StopAllCoroutines();
				StartCoroutine (TweenRoutine (Alpha,from));
				playingForward = false;
			}
			else
			{
				StopAllCoroutines();
				StartCoroutine (TweenRoutine (Alpha,to));
				playingForward = true;
			}
		}
		else
		{
			if(Mathf.Abs (Alpha - from) < Mathf.Abs (Alpha - to))
				//if(image.color.a == from)
				PlayForward();
			else
				PlayReverse ();
		}
	}
	
	public override void Return ()
	{
		base.Return ();
		Alpha = from;
	}

    public override void OnComplete()
    {
        base.OnComplete();
        if (Alpha == from && toggleObject)
            gameObject.SetActive(false);
    }

    public override void SkipToEnd()
    {
        base.SkipToEnd();
        Alpha = to;
        if (recursiveTween)
        {
            foreach (Transform child in transform)
            {
                UITweenAlpha alphaTween = child.GetComponent<UITweenAlpha>();
                if (alphaTween)
                    alphaTween.SkipToEnd();
            }
        }
    }

	protected override IEnumerator TweenRoutine(float tween_From, float tween_To)
	{
		playingForward = tween_To == to;
		float distance = Mathf.Abs (Alpha - tween_To);
		float direction = tween_To - tween_From >= 0 ? 1 : -1;
		
		float rate = 0f;
		float timeElapsed = 0f;
		
		float normalizedDuration = (distance/Mathf.Abs (to-from))*duration;
		float speed = 1/normalizedDuration;
		while(timeElapsed < normalizedDuration)
		{
			Alpha = Mathf.Lerp (Alpha, tween_From+direction*distance*(timeElapsed/normalizedDuration), rate);
			timeElapsed += unscaledTime ? Time.unscaledDeltaTime : Time.deltaTime;
			rate += speed * timeElapsed;
			yield return null;
		}
		Alpha = tween_To;
		
		if(type == TweenType.LOOP)
		{
			if(playingForward)
				Alpha = from;
			else
				Alpha = to;
			
			StartCoroutine (TweenRoutine (tween_From, tween_To));
		}
		else if(type == TweenType.PINGPONG)
		{
			if(playingForward)
				StartCoroutine (TweenRoutine (to,from));
			else
				StartCoroutine (TweenRoutine (from,to));
		}
		else if(type == TweenType.PULSE && !pulsed)
		{
			pulsed = true;
			if(playingForward)
				Alpha = to;
			else
				Alpha = from;
			StartCoroutine (TweenRoutine (tween_To,tween_From));
		}
		else
			OnComplete ();
	}

	public float Alpha
	{
		get
		{
			if(graphic == null && secondaryGraphic == null) 
			{
				Log ("Can't access Alpha, null graphic!");
				return 0f;
			}
			return ThisColor.a;
		}
		set
		{
			if(graphic == null && secondaryGraphic == null)
			{
				Log ("Can't set Alpha, null graphic!");
				return;
			}
			Color newColor = ThisColor;
			newColor.a = value;
			ThisColor = newColor;
		}
	}

	Color ThisColor
	{
		get
		{
			if(graphic)
				return graphic.color;
			else if(secondaryGraphic)
				return secondaryGraphic.color;
			else return new Color();
		}
		set
		{
			if(graphic)
				graphic.color = value;
			else if(secondaryGraphic)
				secondaryGraphic.color = value;
		}
	}

	bool IsValid
	{
		get
		{
			return graphic != null || secondaryGraphic != null;
		}
	}
}
