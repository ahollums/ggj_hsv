﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CanEditMultipleObjects]
[CustomEditor(typeof(CustomTweenScale))]
public class CustomTweenScaleEditor : CustomTweenEditor {

	protected override void SetFrom ()
	{
		CustomTweenScale cts = target as CustomTweenScale;
		cts.from = cts.transform.localScale;
	}

	protected override void SetTo ()
	{
		CustomTweenScale cts = target as CustomTweenScale;
		cts.to = cts.transform.localScale;
	}

	protected override void AssumeFrom ()
	{
		CustomTweenScale cts = target as CustomTweenScale;
		cts.transform.localScale = cts.from;
	}

	protected override void AssumeTo ()
	{
		CustomTweenScale cts = target as CustomTweenScale;
		cts.transform.localScale = cts.to;
	}
}
