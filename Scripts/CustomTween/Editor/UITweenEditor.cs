﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(UITween))]
public class UITweenEditor : Editor {
	protected bool showTools = false;
	
	public override void OnInspectorGUI ()
	{
		base.OnInspectorGUI ();
		showTools = EditorGUILayout.Foldout (showTools, "Inspector Tools");
		if(showTools)
		{
			if(!Application.isPlaying)
			{
				if(GUILayout.Button ("Set from as current value")) SetFrom ();
				if(GUILayout.Button ("Set to as current value")) SetTo();
				if(GUILayout.Button ("Assume value of from")) AssumeFrom ();
				if(GUILayout.Button ("Assume value of to")) AssumeTo ();
			}
			else
			{
				UITween tween = target as UITween;
				GUILayout.BeginHorizontal();
				if(GUILayout.Button ("PlayForward")) tween.PlayForward();
				if(GUILayout.Button ("PlayReverse")) tween.PlayReverse ();
				GUILayout.EndHorizontal();

				GUILayout.BeginHorizontal();
				if(GUILayout.Button ("Stop")) tween.Stop ();
				if(GUILayout.Button ("Return")) tween.Return ();
				GUILayout.EndHorizontal();
			}
		}
	}
	
	protected virtual void SetFrom() {}
	protected virtual void SetTo() {}
	protected virtual void AssumeFrom() {}
	protected virtual void AssumeTo() {}
}
