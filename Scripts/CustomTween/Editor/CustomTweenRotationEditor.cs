﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CanEditMultipleObjects]
[CustomEditor(typeof(CustomTweenRotation))]
public class CustomTweenRotationEditor : CustomTweenEditor {

	protected override void SetFrom ()
	{
		CustomTweenRotation ctr = target as CustomTweenRotation;
		ctr.from = ctr.transform.localEulerAngles;
	}
	
	protected override void SetTo ()
	{
		CustomTweenRotation ctr = target as CustomTweenRotation;
		ctr.to = ctr.transform.localEulerAngles;
	}
	
	protected override void AssumeFrom ()
	{
		CustomTweenRotation ctr = target as CustomTweenRotation;
		ctr.transform.localEulerAngles = ctr.from;
	}
	
	protected override void AssumeTo ()
	{
		CustomTweenRotation ctr = target as CustomTweenRotation;
		ctr.transform.localEulerAngles = ctr.to;
	}
}
