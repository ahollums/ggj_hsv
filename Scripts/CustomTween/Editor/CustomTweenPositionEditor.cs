﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CanEditMultipleObjects]
[CustomEditor(typeof(CustomTweenPosition))]
public class CustomTweenPositionEditor : CustomTweenEditor {

	protected override void SetFrom ()
	{
		CustomTweenPosition ctp = target as CustomTweenPosition;
		ctp.from = ctp.transform.localPosition;
	}
	
	protected override void SetTo ()
	{
		CustomTweenPosition ctp = target as CustomTweenPosition;
		ctp.to = ctp.transform.localPosition;
	}
	
	protected override void AssumeFrom ()
	{
		CustomTweenPosition ctp = target as CustomTweenPosition;
		ctp.transform.localPosition = ctp.from;
	}
	
	protected override void AssumeTo ()
	{
		CustomTweenPosition ctp = target as CustomTweenPosition;
		ctp.transform.localPosition = ctp.to;
	}
}
