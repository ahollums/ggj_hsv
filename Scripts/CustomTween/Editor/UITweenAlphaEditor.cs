﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(UITweenAlpha))]
public class UITweenAlphaEditor : UITweenEditor {

	void OnEnable()
	{
		UITweenAlpha uta = target as UITweenAlpha;
	
		uta.graphic = uta.gameObject.GetComponent<SpriteRenderer>();
		if(uta.graphic == null) uta.secondaryGraphic = uta.gameObject.GetComponent<UnityEngine.UI.MaskableGraphic>();
	}

	protected override void SetTo ()
	{
		UITweenAlpha uta = target as UITweenAlpha;
		if(uta && uta.graphic)
		{
			uta.to = uta.graphic.color.a;
		}
	}

	protected override void SetFrom ()
	{
		UITweenAlpha uta = target as UITweenAlpha;
		if(uta && uta.graphic)
		{
			uta.from = uta.graphic.color.a;
		}
	}

	protected override void AssumeTo ()
	{
		UITweenAlpha uta = target as UITweenAlpha;
		if(uta && uta.graphic)
		{
			Color newColor = uta.graphic.color;
			newColor.a = uta.to;
			uta.graphic.color = newColor;
		}
	}

	protected override void AssumeFrom ()
	{
		UITweenAlpha uta = target as UITweenAlpha;
		if(uta && uta.graphic)
		{
			Color newColor = uta.graphic.color;
			newColor.a = uta.from;
			uta.graphic.color = newColor;
		}
	}
}
