﻿using UnityEngine;
using System.Collections;

public class InteractiveTweenRotation : InteractiveTween 
{
	public Vector3 from = Vector3.zero;
	public Vector3 to = Vector3.zero;
	
	public override void PlayForward ()
	{
		if(isPlaying)
		{
			base.PlayForward ();
			StartCoroutine (TweenRoutine (transform.localEulerAngles,to));
		}
		else
		{
			base.PlayForward ();
			transform.localEulerAngles = from;
			StartCoroutine (TweenRoutine(from, to));
		}
	}
	
	public override void PlayReverse ()
	{
		if(isPlaying)
		{
			base.PlayReverse ();
			StartCoroutine (TweenRoutine (transform.localEulerAngles, from));
		}
		else
		{
			base.PlayReverse ();
			transform.localEulerAngles = to;
			StartCoroutine (TweenRoutine(to, from));
		}
	}
	
	public override void Toggle()
	{
		if(type == TweenType.LOOP)
		{
			if(playingForward)
				PlayReverse ();
			else
				PlayForward ();
			return;
		}
		
		if(isPlaying)
		{
			if(playingForward)
			{
				StopAllCoroutines();
				StartCoroutine (TweenRoutine (transform.localEulerAngles,from));
				playingForward = false;
			}
			else
			{
				StopAllCoroutines();
				StartCoroutine (TweenRoutine (transform.localEulerAngles,to));
				playingForward = true;
			}
		}
		else
		{
			if(transform.localEulerAngles == from)
				PlayForward();
			else
				PlayReverse ();
		}
	}
	
	public override void Return ()
	{
		base.Return ();
		transform.localEulerAngles = from;
	}
	
	protected override IEnumerator TweenRoutine(Vector3 tween_From, Vector3 tween_To)
	{
		playingForward = tween_To == to;
		Quaternion q_To = Quaternion.Euler (tween_To);
		float distance = Vector3.Distance (transform.localEulerAngles, tween_To);
		float rate = 0f;
		float timeElapsed = 0f;
		float normalizedDuration = (distance/Vector3.Distance (to,from))*duration;
		float speed = 1/normalizedDuration;
		while(timeElapsed < normalizedDuration)
		{
			transform.localRotation = Quaternion.Slerp (transform.localRotation, q_To, timeElapsed/normalizedDuration);
			//transform.localEulerAngles = Vector3.Lerp (transform.localEulerAngles, tween_From+direction*distance*(timeElapsed/normalizedDuration), rate);
			timeElapsed += Time.deltaTime;
			rate += speed * timeElapsed;
			yield return null;
		}
		transform.localRotation = q_To;
		
		if(type == TweenType.LOOP)
		{
			if(playingForward)
				transform.localEulerAngles = from;
			else
				transform.localEulerAngles = to;
			
			StartCoroutine (TweenRoutine (tween_From, tween_To));
		}
		else if(type == TweenType.PINGPONG)
		{
			if(playingForward)
				StartCoroutine (TweenRoutine (to,from));
			else
				StartCoroutine (TweenRoutine (from,to));
		}
		else if(type == TweenType.PULSE && !pulsed)
		{
			pulsed = true;
			if(playingForward)
				transform.localEulerAngles = to;
			else
				transform.localEulerAngles = from;
			StartCoroutine (TweenRoutine (tween_To,tween_From));
		}
		else
			OnComplete ();
	}
}
