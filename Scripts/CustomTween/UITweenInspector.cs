﻿using UnityEngine;
using System.Collections;

public class UITweenInspector : ScriptableObject
{
	public UITweenType type;
	[HideInInspector]
	public Vector3 m_v3From;
	[HideInInspector]
	public Vector3 m_v3To;
	[HideInInspector]
	public float m_fFrom;
	[HideInInspector]
	public float m_fTo;
	[HideInInspector]
	public Color m_cFrom;
	[HideInInspector]
	public Color m_cTo;
	
	public float duration;
	public RectTransform tweenTarget;
}