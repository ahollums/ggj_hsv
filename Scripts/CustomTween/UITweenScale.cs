﻿using UnityEngine;
using System.Collections;

public class UITweenScale : UITween {
	Vector3 from = Vector3.zero;
	public Vector3 to = Vector3.zero;
	
	protected override void Start ()
	{
		base.Start ();
		from = UIObject.localScale;
		to = new Vector3(UIObject.localScale.x * to.x, UIObject.localScale.y * to.y, UIObject.localScale.z * to.z);
	}
	
	public override void PlayForward ()
	{
		if(isPlaying)
		{
			base.PlayForward ();
			StartCoroutine (TweenRoutine (UIObject.localScale,to));
		}
		else
		{
			base.PlayForward ();
			UIObject.localScale = from;
			StartCoroutine (TweenRoutine(from, to));
		}
	}
	
	public override void PlayReverse ()
	{
		if(isPlaying)
		{
			base.PlayReverse ();
			StartCoroutine (TweenRoutine (UIObject.localScale, from));
		}
		else
		{
			base.PlayReverse ();
			UIObject.localScale = to;
			StartCoroutine (TweenRoutine(to, from));
		}
	}
	public override void Toggle()
	{
		if(type == TweenType.LOOP)
		{
			if(playingForward)
				PlayReverse ();
			else
				PlayForward ();
			return;
		}
		
		if(isPlaying)
		{
			if(playingForward)
			{
				StopAllCoroutines();
				StartCoroutine (TweenRoutine (UIObject.localScale,from));
				playingForward = false;
			}
			else
			{
				StopAllCoroutines();
				StartCoroutine (TweenRoutine (UIObject.localScale,to));
				playingForward = true;
			}
		}
		else
		{
			if(UIObject.localScale == from)
				PlayForward();
			else
				PlayReverse ();
		}
	}
	
	
	public override void Return ()
	{
		base.Return ();
		UIObject.localScale = from;
	}
	
	protected override IEnumerator TweenRoutine (Vector3 tween_From, Vector3 tween_To)
	{
		playingForward = tween_To == to;
		
		float distance = Vector3.Distance (UIObject.localScale, tween_To);
		Vector3 direction = (tween_To - tween_From);
		direction.Normalize ();
		
		float rate = 0f;
		float timeElapsed = 0f;
		float normalizedDuration = (distance/Vector3.Distance (to,from))*duration;
		float speed = 1/normalizedDuration;
		
		while(timeElapsed < normalizedDuration)
		{
			UIObject.localScale = Vector3.Lerp (UIObject.localScale, tween_From+direction*distance*(timeElapsed/normalizedDuration), rate);
			timeElapsed += unscaledTime ? Time.unscaledDeltaTime : Time.deltaTime;
			rate += speed * timeElapsed;
			yield return null;
		}
		UIObject.localScale = tween_To;
		
		if(type == TweenType.LOOP)
		{
			if(playingForward)
				UIObject.localScale = from;
			else
				UIObject.localScale = to;
			
			StartCoroutine (TweenRoutine (tween_From, tween_To));
		}
		else if(type == TweenType.PINGPONG)
		{
			if(playingForward)
				StartCoroutine (TweenRoutine (to,from));
			else
				StartCoroutine (TweenRoutine (from,to));
		}
		else if(type == TweenType.PULSE && !pulsed)
		{
			if(playingForward)
				UIObject.localScale = to;
			else
				UIObject.localScale = from;
			pulsed = true;
			StartCoroutine (TweenRoutine (tween_To,tween_From));
		}
		else
			OnComplete ();
	}
}
