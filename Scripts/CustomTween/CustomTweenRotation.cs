﻿using UnityEngine;
using System.Collections;

public class CustomTweenRotation : CustomTween 
{
	public Vector3 from = Vector3.zero;
	public Vector3 to = Vector3.zero;
	
	public override void PlayForward ()
	{
		if(isPlaying)
		{
			base.PlayForward ();
			StartCoroutine (TweenRoutine (CurrentValue,to));
		}
		else
		{
			base.PlayForward ();
			CurrentValue = from;
			StartCoroutine (TweenRoutine(from, to));
		}
	}
	
	public override void PlayReverse ()
	{
		if(isPlaying)
		{
			base.PlayReverse ();
			StartCoroutine (TweenRoutine (CurrentValue, from));
		}
		else
		{
			base.PlayReverse ();
			CurrentValue = to;
			StartCoroutine (TweenRoutine(to, from));
		}
	}
	
	public override void Toggle()
	{
		if(type == TweenType.LOOP)
		{
			if(playingForward)
				PlayReverse ();
			else
				PlayForward ();
			return;
		}
		
		if(isPlaying)
		{
			if(playingForward)
			{
				StopAllCoroutines();
				StartCoroutine (TweenRoutine (CurrentValue,from));
				playingForward = false;
			}
			else
			{
				StopAllCoroutines();
				StartCoroutine (TweenRoutine (CurrentValue,to));
				playingForward = true;
			}
		}
		else
		{
			if(CurrentValue == from)
				PlayForward();
			else
				PlayReverse ();
		}
	}
	
	public override void Return ()
	{
		base.Return ();
		CurrentValue = from;
	}

	public override void SkipToEnd ()
	{
		base.SkipToEnd ();
		CurrentValue = to;
	}
	
	protected override IEnumerator TweenRoutine(Vector3 tween_From, Vector3 tween_To)
	{
		playingForward = tween_To == to;
		Quaternion q_To = Quaternion.Euler (tween_To);
		float distance = Vector3.Distance (CurrentValue, tween_To);
		float rate = 0f;
		float timeElapsed = 0f;
		float normalizedDuration = (distance/Vector3.Distance (to,from))*duration;
		float speed = 1/normalizedDuration;
		while(timeElapsed < normalizedDuration)
		{
			transform.localRotation = Quaternion.Slerp (transform.localRotation, q_To, timeElapsed/normalizedDuration);
			//CurrentValue = Vector3.Lerp (CurrentValue, tween_From+direction*distance*(timeElapsed/normalizedDuration), rate);
			timeElapsed += unscaledTime ? Time.unscaledDeltaTime : Time.deltaTime;
			rate += speed * timeElapsed;
			yield return null;
		}
		transform.localRotation = q_To;
		
		if(type == TweenType.LOOP)
		{
			if(playingForward)
				CurrentValue = from;
			else
				CurrentValue = to;
			
			StartCoroutine (TweenRoutine (tween_From, tween_To));
		}
		else if(type == TweenType.PINGPONG)
		{
			if(playingForward)
				StartCoroutine (TweenRoutine (to,from));
			else
				StartCoroutine (TweenRoutine (from,to));
		}
		else if(type == TweenType.PULSE && !pulsed)
		{
			pulsed = true;
			if(playingForward)
				CurrentValue = to;
			else
				CurrentValue = from;
			StartCoroutine (TweenRoutine (tween_To,tween_From));
		}
		else
			OnComplete ();
	}

	// TODO: Work CurrentValue in everywhere that calls transform.localEuler
	public Vector3 CurrentValue
	{
		get
		{
			return transform.localEulerAngles;
		}
		set
		{
			transform.localEulerAngles = value;
		}
	}

	public override bool AtToValue {
		get {
			return CurrentValue == to;
		}
	}
}
