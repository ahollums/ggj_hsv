﻿using UnityEngine;
using System.Collections;

public class CustomTweenVectorAxis : CustomTween {
	[System.Serializable]
	public enum VectorAxis {
		X=0,
		Y,
		Z
	};

	public VectorAxis vectorAxis;
	public float fromVal = 0f;
	public float toVal = 0f;

	protected override void Start()
	{
		base.Start();
	}

	public override void PlayForward ()
	{
		if(isPlaying)
		{
			base.PlayForward ();
			Vector3 to = transform.localPosition;
			switch (vectorAxis)
			{
				case VectorAxis.X:
					to.x = toVal;
					break;
				case VectorAxis.Y:
					to.y = toVal;
					break;
				case VectorAxis.Z:
					to.z = toVal;
					break;
			}

			StartCoroutine (TweenRoutine(transform.localPosition, to));
		}
		else
		{
			base.PlayForward ();
			Vector3 to = transform.localPosition;
			Vector3 from = transform.localPosition;
			switch (vectorAxis)
			{
				case VectorAxis.X:
					from.x = fromVal;
					to.x = toVal;
					break;
				case VectorAxis.Y:
					from.y = fromVal;
					to.y = toVal;
					break;
				case VectorAxis.Z:
					from.z = fromVal;
					to.z = toVal;
					break;
			}

			transform.localPosition = from;
			StartCoroutine (TweenRoutine(from, to));
		}
	}
	
	public override void PlayReverse ()
	{
		if(isPlaying)
		{
			base.PlayReverse ();
			Vector3 from = transform.localPosition;
			switch (vectorAxis)
			{
				case VectorAxis.X:
					from.x = fromVal;
					break;
				case VectorAxis.Y:
					from.y = fromVal;
					break;
				case VectorAxis.Z:
					from.z = fromVal;
					break;
			}
			StartCoroutine (TweenRoutine (transform.localPosition, from));
		}
		else
		{
			base.PlayReverse ();
			Vector3 to = transform.localPosition;
			Vector3 from = transform.localPosition;
			switch (vectorAxis)
			{
				case VectorAxis.X:
					from.x = fromVal;
					to.x = toVal;
					break;
				case VectorAxis.Y:
					from.y = fromVal;
					to.y = toVal;
					break;
				case VectorAxis.Z:
					from.z = fromVal;
					to.z = toVal;
					break;
			}
			transform.localPosition = to;
			StartCoroutine (TweenRoutine(to, from));
		}
	}
	
	public override void Toggle()
	{
		if(type == TweenType.LOOP)
		{
			if(playingForward)
				PlayReverse ();
			else
				PlayForward ();
			return;
		}
		
		if(isPlaying)
		{
			if(playingForward)
			{
				StopAllCoroutines();
				Vector3 from = transform.localPosition;
				switch (vectorAxis)
				{
					case VectorAxis.X:
						from.x = fromVal;
						break;
					case VectorAxis.Y:
						from.y = fromVal;
						break;
					case VectorAxis.Z:
						from.z = fromVal;
						break;
				}
				StartCoroutine (TweenRoutine (transform.localPosition,from));
				playingForward = false;
			}
			else
			{
				StopAllCoroutines();
				Vector3 to = transform.localPosition;
				switch (vectorAxis)
				{
					case VectorAxis.X:
						to.x = toVal;
						break;
					case VectorAxis.Y:
						to.y = toVal;
						break;
					case VectorAxis.Z:
						to.z = toVal;
						break;
				}
				StartCoroutine (TweenRoutine (transform.localPosition,to));
				playingForward = true;
			}
		}
		else
		{
			Vector3 from = transform.localPosition;
			switch (vectorAxis)
			{
				case VectorAxis.X:
					from.x = fromVal;
					break;
				case VectorAxis.Y:
					from.y = fromVal;
					break;
				case VectorAxis.Z:
					from.z = fromVal;
					break;
			}
			if (transform.localPosition == from)
				PlayForward();
			else
				PlayReverse ();
		}
	}
	
	public override void Return ()
	{
		base.Return ();
		Vector3 from = transform.localPosition;
		switch (vectorAxis)
		{
			case VectorAxis.X:
				from.x = fromVal;
				break;
			case VectorAxis.Y:
				from.y = fromVal;
				break;
			case VectorAxis.Z:
				from.z = fromVal;
				break;
		}
		transform.localPosition = from;
	}
	
	protected override IEnumerator TweenRoutine(Vector3 tween_From, Vector3 tween_To)
	{
		Vector3 to = transform.localPosition;
		Vector3 from = transform.localPosition;
		switch (vectorAxis)
		{
			case VectorAxis.X:
				from.x = fromVal;
				to.x = toVal;
				break;
			case VectorAxis.Y:
				from.y = fromVal;
				to.y = toVal;
				break;
			case VectorAxis.Z:
				from.z = fromVal;
				to.z = toVal;
				break;
		}

		playingForward = tween_To == to;
		float distance = Vector3.Distance (transform.localPosition, tween_To);
		Vector3 direction = (tween_To - tween_From);
		direction.Normalize ();
		
		float rate = 0f;
		float timeElapsed = 0f;
		
		float normalizedDuration = (distance/Vector3.Distance (to,from))*duration;
		float speed = 1/normalizedDuration;
		while(timeElapsed < normalizedDuration)
		{
			transform.localPosition = Vector3.Lerp (transform.localPosition, tween_From+direction*distance*(timeElapsed/normalizedDuration), rate);
			timeElapsed += Time.deltaTime;
			rate += speed * timeElapsed;
			yield return null;
		}
		transform.localPosition = tween_To;
		
		if(type == TweenType.LOOP)
		{
			if(playingForward)
				transform.localPosition = from;
			else
				transform.localPosition = to;
			
			StartCoroutine (TweenRoutine (tween_From, tween_To));
		}
		else if(type == TweenType.PINGPONG)
		{
			if(playingForward)
				StartCoroutine (TweenRoutine (to,from));
			else
				StartCoroutine (TweenRoutine (from,to));
		}
		else if(type == TweenType.PULSE && !pulsed)
		{
			pulsed = true;
			if(playingForward)
				transform.localPosition = to;
			else
				transform.localPosition = from;
			StartCoroutine (TweenRoutine (tween_To,tween_From));
		}
		else
			OnComplete ();
	}
}
