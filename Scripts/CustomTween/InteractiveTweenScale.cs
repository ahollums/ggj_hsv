﻿using UnityEngine;
using System.Collections;

public class InteractiveTweenScale : InteractiveTween {
	public Vector3 from = Vector3.zero;
	public Vector3 to = Vector3.zero;


	protected override void Start ()
	{
		base.Start ();
	}

	public override void PlayForward ()
	{
		if(isPlaying)
		{
			base.PlayForward ();
			StartCoroutine (TweenRoutine (transform.localScale,to));
		}
		else
		{
			base.PlayForward ();
			transform.localScale = from;
			StartCoroutine (TweenRoutine(from, to));
		}
	}
	
	public override void PlayReverse ()
	{
		if(isPlaying)
		{
			base.PlayReverse ();
			StartCoroutine (TweenRoutine (transform.localScale, from));
		}
		else
		{
			base.PlayReverse ();
			transform.localScale = to;
			StartCoroutine (TweenRoutine(to, from));
		}
	}
	public override void Toggle()
	{
		if(type == TweenType.LOOP)
		{
			if(playingForward)
				PlayReverse ();
			else
				PlayForward ();
			return;
		}

		if(isPlaying)
		{
			if(playingForward)
			{
				StopAllCoroutines();
				StartCoroutine (TweenRoutine (transform.localScale,from));
				playingForward = false;
			}
			else
			{
				StopAllCoroutines();
				StartCoroutine (TweenRoutine (transform.localScale,to));
				playingForward = true;
			}
		}
		else
		{
			if(transform.localScale == from)
				PlayForward();
			else
				PlayReverse ();
		}
	}


	public override void Return ()
	{
		base.Return ();
		transform.localScale = from;
	}

	protected override IEnumerator TweenRoutine (Vector3 tween_From, Vector3 tween_To)
	{
		playingForward = tween_To == to;

		float distance = Vector3.Distance (transform.localScale, tween_To);
		Vector3 direction = (tween_To - tween_From);
		direction.Normalize ();
		
		float rate = 0f;
		float timeElapsed = 0f;
		float normalizedDuration = (distance/Vector3.Distance (to,from))*duration;
		float speed = 1/normalizedDuration;

		while(timeElapsed < normalizedDuration)
		{
			transform.localScale = Vector3.Lerp (transform.localScale, tween_From+direction*distance*(timeElapsed/normalizedDuration), rate);
			timeElapsed += Time.deltaTime;
			rate += speed * timeElapsed;
			yield return null;
		}
		transform.localScale = tween_To;
		
		if(type == TweenType.LOOP)
		{
			if(playingForward)
				transform.localScale = from;
			else
				transform.localScale = to;
			
			StartCoroutine (TweenRoutine (tween_From, tween_To));
		}
		else if(type == TweenType.PINGPONG)
		{
			if(playingForward)
				StartCoroutine (TweenRoutine (to,from));
			else
				StartCoroutine (TweenRoutine (from,to));
		}
		else if(type == TweenType.PULSE && !pulsed)
		{
			if(playingForward)
				transform.localScale = to;
			else
				transform.localScale = from;
			pulsed = true;
			StartCoroutine (TweenRoutine (tween_To,tween_From));
		}
		else
			OnComplete ();
	}
}
