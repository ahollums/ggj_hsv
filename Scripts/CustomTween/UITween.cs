﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

[System.Serializable]
public enum UITweenType
{
	NONE=0,
	SCALE,
	ALPHA,
	COLOR,
	POSITION
}

[RequireComponent(typeof (RectTransform))]
public class UITween : MonoBehaviour 
{
	protected RectTransform UIObject;
	[Tooltip("The style of Tween [See TweenType declaration in CustomTween class]")]
	public TweenType type;
	[Tooltip("Defines if the tween is played from some kind of cue from the user, i.e. click / hover.")]
	public InteractType listensTo;

	[Tooltip("Specifies how long the tween takes to play one iteration.")]
	public float duration = 1f;
	
	protected bool isPlaying = false;
	protected bool playingForward = false;
	protected bool pulsed = false;
	
	public delegate void TweenEventDelegate();
	public TweenEventDelegate onComplete;
	public TweenEventDelegate onForward;
	public TweenEventDelegate onReverse;
	public TweenEventDelegate onStop;

	[Tooltip("Play this tween as soon as the application starts or its GameObject is first enabled.")]
	public bool playOnStart = false;

    [Tooltip("Reset this tween to its beginning state on start.")]
    public bool resetOnStart = false;

    public bool playOnEnable = false;

	[Tooltip("Play this tween independent of timescale")]
	public bool unscaledTime = false;

	[Tooltip("Show debug log comments.")]
	public bool logging = false;
    public bool toggleObject = false;
	CustomButton customButton;
	
    protected virtual void Awake()
    {
        
    }

	protected virtual void Start()
	{
		UIObject = GetComponent<RectTransform>();
		customButton = GetComponent<CustomButton>();
		if(playOnStart)
			PlayForward ();
		SetupListener ();
        if (resetOnStart)
            Return();
	}

	protected void SetupListener()
	{
		if(customButton == null) return;
		switch(listensTo)
		{
		case InteractType.NONE:
			break;
		case InteractType.CLICK:
			customButton.onMouseDown+=PlayForward;
			if(type == TweenType.LOOP || type == TweenType.PINGPONG || type == TweenType.PULSE)
				customButton.onMouseUp+=Return;
			else
				customButton.onMouseUp+=PlayReverse;
			break;
//		case InteractType.DRAG:
//			customButton.onDragStart+=PlayForward;
//			if(type == TweenType.LOOP || type == TweenType.PINGPONG || type == TweenType.PULSE)
//				customButton.onDragRelease+=Return;
//			else
//				customButton.onDragRelease+=PlayReverse;
//			break;
		case InteractType.HOVER:
			customButton.onMouseEnter+=PlayForward;
			if(type == TweenType.LOOP || type == TweenType.PINGPONG || type == TweenType.PULSE)
				customButton.onMouseLeave+=Return;
			else
				customButton.onMouseLeave+=PlayReverse;
			break;
		default:
			break;
		}
	}

    void OnEnable()
    {
        if(playOnEnable)
        {
            Return();
            PlayForward();
        }
    }

	public virtual void PlayForward() 
	{ 
        if (toggleObject)
            gameObject.SetActive(true);
		Log ("PlayForward()");
		StopAllCoroutines();
		isPlaying = true;
		playingForward = true;
		pulsed = false;

		if(onForward != null) onForward();
	}
	
	public virtual void PlayReverse() 
	{ 
		Log ("PlayReverse()");
		StopAllCoroutines();
		isPlaying = true;
		playingForward = false;
		pulsed = false;
		if(onReverse != null) onReverse();
	}
	
	public virtual void Stop() 
	{ 
		StopAllCoroutines();
		isPlaying = false;
		playingForward = false;
		pulsed = false;
		if(onStop != null) onStop();
	}
	
	public virtual void Toggle()
	{
		
	}
	
	// Should call to stop and go back to origin
	public virtual void Return()
	{
		Stop ();
	}

    public virtual void SkipToEnd()
    {
        
    }
	
	public virtual void OnComplete()
	{
		isPlaying = false;
        if (onComplete != null)
            onComplete();
	}
	
	protected virtual IEnumerator TweenRoutine(Vector3 tween_From, Vector3 tween_To) { yield return null; }
	protected virtual IEnumerator TweenRoutine(float tween_From, float tween_To) {yield return null;}
	
	public bool IsPlaying
	{
		get
		{
			return isPlaying;
		}
	}
	
	protected virtual void Log(string message)
	{
		if(logging)
			Debug.Log ("[CustomTween]: "+message);
	}
}

