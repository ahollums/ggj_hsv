using UnityEngine;
using System.Collections;
using System;

public class InteractiveTween : InteractiveObject 
{
	public TweenType type;
	public InteractType listensTo;

	public float duration = 1f;
	
	protected bool isPlaying = false;
	protected bool playingForward = false;
	protected bool pulsed = false;

	public delegate void OnCompleteDelegate();
	public OnCompleteDelegate onComplete;

	public bool playOnStart = false;

	protected virtual void Start()
	{
		if(playOnStart)
			PlayForward ();

		SetupListener ();
	}

	protected void SetupListener()
	{
		switch(listensTo)
		{
		case InteractType.NONE:
			break;
		case InteractType.CLICK:
			onMouseDown+=PlayForward;
			if(type == TweenType.LOOP || type == TweenType.PINGPONG || type == TweenType.PULSE)
				onMouseUp+=Return;
			else
				onMouseUp+=PlayReverse;
			break;
		case InteractType.DRAG:
			onDragStart+=PlayForward;
			if(type == TweenType.LOOP || type == TweenType.PINGPONG || type == TweenType.PULSE)
				onDragRelease+=Return;
			else
				onDragRelease+=PlayReverse;
			break;
		case InteractType.HOVER:
			onMouseOver+=PlayForward;
			if(type == TweenType.LOOP || type == TweenType.PINGPONG || type == TweenType.PULSE)
				onMouseExit+=Return;
			else
				onMouseExit+=PlayReverse;
			break;
		default:
			break;
		}
	}
	
	public virtual void PlayForward() 
	{ 
		Log ("PlayForward()");
		StopAllCoroutines();
		isPlaying = true;
		playingForward = true;
		pulsed = false;
	}

	public virtual void PlayReverse() 
	{ 
		Log ("PlayReverse()");
		StopAllCoroutines();
		isPlaying = true;
		playingForward = false;
		pulsed = false;
	}

	public virtual void Stop() 
	{ 
		StopAllCoroutines();
		isPlaying = false;
		playingForward = false;
		pulsed = false;
	}

	public virtual void Toggle()
	{

	}

	// Should call to stop and go back to origin
	public virtual void Return()
	{
		Stop ();
	}

	public virtual void OnComplete()
	{
		isPlaying = false;
		if(onComplete!=null)
			onComplete();
	}

	protected virtual IEnumerator TweenRoutine(Vector3 tween_From, Vector3 tween_To) { yield return null; }
	protected virtual IEnumerator TweenRoutine(float tween_From, float tween_To) {yield return null;}

	public bool IsPlaying
	{
		get
		{
			return isPlaying;
		}
	}

	protected override void Log(string message)
	{
		if(logging)
			Debug.Log ("[CustomTween]: "+message);
	}
}

