﻿using UnityEngine;
using System.Collections;
using System;

[System.Serializable]
public enum TweenType
{
	ONESHOT=0, 		// Performs the tween once per call.
	LOOP, 			// Continuously repeat the tween until told to stop.
	PINGPONG, 		// Continuously cycle back and forth until told to stop.
	PULSE 			// Performs the tween forward, then backward once per call.
}

[System.Serializable]
public enum TweenSelection
{
	POSITION = 0,
	ROTATION,
	SCALE,
	ALPHA
}

public class CustomTween : MonoBehaviour 
{
	[Tooltip("The style of Tween [See TweenType declaration in CustomTween class]")]
	public TweenType type;

	[Tooltip("Specifies how long the tween takes to play one iteration.")]
	public float duration = 1f;
	
	protected bool isPlaying = false;
	protected bool playingForward = false;
	protected bool pulsed = false;
	
	public delegate void VoidDelegate();
	public VoidDelegate onComplete;
	public VoidDelegate onForwardComplete;
	public VoidDelegate onReverseComplete;
	public VoidDelegate onForward;
	public VoidDelegate onReverse;

	[Tooltip("Play this tween as soon as the application starts or its GameObject is first enabled.")]
	public bool playOnStart = false;

    [Tooltip("Play this tween OnEnable()")]
    public bool playOnEnable = false;

	[Tooltip("Play this tween independent of timescale")]
	public bool unscaledTime = false;

	[Tooltip("Show debug log comments.")]
	public bool logging = false;
	
	protected virtual void Start()
	{
		if(playOnStart)
			PlayForward ();
	}

	public virtual void PlayForward() 
	{ 
		Log ("PlayForward()");
		StopAllCoroutines();
		isPlaying = true;
		playingForward = true;
		pulsed = false;
		if(onForward != null) onForward();
	}
	
	public virtual void PlayReverse() 
	{ 
		Log ("PlayReverse()");
		StopAllCoroutines();
		isPlaying = true;
		playingForward = false;
		pulsed = false;
		if(onReverse != null) onReverse();
	}
	
	public virtual void Stop() 
	{ 
		StopAllCoroutines();
		isPlaying = false;
		playingForward = false;
		pulsed = false;
	}
	
	public virtual void Toggle()
	{
		
	}

    void OnEnable()
    {
        if (playOnEnable)
        {
            Return();
            PlayForward();
        }
    }
	
	// Should call to stop and go back to origin
	public virtual void Return()
	{
		Stop ();
	}

	public virtual void SkipToEnd()
	{
		
	}
	
	public virtual void OnComplete()
	{
		isPlaying = false;
		if(onComplete!=null)
			onComplete();
		if(AtToValue && onForwardComplete != null) onForwardComplete();
		else if(!AtToValue && onReverseComplete != null) onReverseComplete();
	}

	public virtual bool AtToValue { get
		{
			Log("Using the base 'AtToValue!  Implement this in derived classes!");
			return true;
		} 
	}
	
	protected virtual IEnumerator TweenRoutine(Vector3 tween_From, Vector3 tween_To) { yield return null; }
	protected virtual IEnumerator TweenRoutine(float tween_From, float tween_To) {yield return null;}
	
	public bool IsPlaying
	{
		get
		{
			return isPlaying;
		}
	}
	
	protected virtual void Log(string message)
	{
		if(logging)
			Debug.Log ("[CustomTween]: "+message);
	}
}

