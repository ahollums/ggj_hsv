﻿using UnityEngine;
using System.Collections;

public class UITweenPosition : UITween {

	public Vector3 from = Vector3.zero;
	public Vector3 to = Vector3.zero;

	public override void PlayForward ()
	{
		if(UIObject.position == to) return;
		if(isPlaying)
		{
			base.PlayForward ();
			StartCoroutine (TweenRoutine (UIObject.position,to));
		}
		else
		{
			base.PlayForward ();
			UIObject.position = from;
			StartCoroutine (TweenRoutine(from, to));
		}
	}
	
	public override void PlayReverse ()
	{
		if(UIObject.position == from) return;
		if(isPlaying)
		{
			base.PlayReverse ();
			StartCoroutine (TweenRoutine (UIObject.position, from));
		}
		else
		{
			base.PlayReverse ();
			UIObject.position = to;
			StartCoroutine (TweenRoutine(to, from));
		}
	}
	
	public override void Toggle()
	{
		if(type == TweenType.LOOP)
		{
			if(playingForward)
				PlayReverse ();
			else
				PlayForward ();
			return;
		}
		
		if(isPlaying)
		{
			if(playingForward)
			{
				StopAllCoroutines();
				StartCoroutine (TweenRoutine (UIObject.position,from));
				playingForward = false;
			}
			else
			{
				StopAllCoroutines();
				StartCoroutine (TweenRoutine (UIObject.position,to));
				playingForward = true;
			}
		}
		else
		{
			if(Vector3.Distance (UIObject.position, from) < Vector3.Distance (UIObject.position,to))
			//if(UIObject.position == from)
				PlayForward();
			else
				PlayReverse ();
		}
	}
	
	public override void Return ()
	{
		base.Return ();
		UIObject.position = from;
	}
	
	protected override IEnumerator TweenRoutine(Vector3 tween_From, Vector3 tween_To)
	{
		playingForward = tween_To == to;
		float distance = Vector3.Distance (UIObject.position, tween_To);
		Vector3 direction = (tween_To - tween_From);
		direction.Normalize ();
		
		float rate = 0f;
		float timeElapsed = 0f;
		
		float normalizedDuration = (distance/Vector3.Distance (to,from))*duration;
		float speed = 1/normalizedDuration;
		while(timeElapsed < normalizedDuration)
		{
			UIObject.position = Vector3.Lerp (UIObject.position, tween_From+direction*distance*(timeElapsed/normalizedDuration), rate);
			timeElapsed += unscaledTime ? Time.unscaledDeltaTime : Time.deltaTime;
			rate += speed * timeElapsed;
			yield return null;
		}
		UIObject.position = tween_To;
		
		if(type == TweenType.LOOP)
		{
			if(playingForward)
				UIObject.position = from;
			else
				UIObject.position = to;
			
			StartCoroutine (TweenRoutine (tween_From, tween_To));
		}
		else if(type == TweenType.PINGPONG)
		{
			if(playingForward)
				StartCoroutine (TweenRoutine (to,from));
			else
				StartCoroutine (TweenRoutine (from,to));
		}
		else if(type == TweenType.PULSE && !pulsed)
		{
			pulsed = true;
			if(playingForward)
				UIObject.position = to;
			else
				UIObject.position = from;
			StartCoroutine (TweenRoutine (tween_To,tween_From));
		}
		else
			OnComplete ();
	}
}
